package com.NortonWebsite.utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;



public class PropertiesFile {
	
	public static WebDriver driver = null;
    public static WebDriverWait wait;
	
    public static String Browser;
	public static String url;
	public static String promotionalurl;
	public static String directurl;
	public static String DriverPath;
	
	
    public WebDriver getDriver() {
        return driver;
    }
	
    
    // Read Browser name, Test url and Driver file path from config.properties.
    
	public static void readPropertiesFile() throws Exception {
		
		Properties prop = new Properties();
		
		try {
			
			InputStream input = new FileInputStream("/Users/ashishdewalkar/Documents/eclipse-workspace/NortonWebsite/src/test/resources/config.properties");
			prop.load(input);
			
			Browser = prop.getProperty("browsername");
			url = prop.getProperty("testurl");
			promotionalurl = prop.getProperty("promotionaltesturl");
			directurl = prop.getProperty("directtesturl");
			DriverPath = prop.getProperty("driverfile");

			
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
	}	

	
	// Set Browser configurations by comparing Browser name and Diver file path.
	
	public static void setBrowserConfig() {
		
			if(Browser.contains("Firefox")) {
				System.setProperty("webdriver.gecko.driver", DriverPath + "/geckodriver");
            
				FirefoxOptions firefoxOptions = new FirefoxOptions();
				firefoxOptions.setHeadless(false);
				firefoxOptions.setCapability("marionette", true);
            
				driver = new FirefoxDriver(firefoxOptions); 
            
			}
			
			if(Browser.contains("Chrome")) {
				System.setProperty("webdriver.chrome.driver", DriverPath + "/chromedriver");
			
				DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
				ChromeOptions chromeOptions = new ChromeOptions();
				//chromeOptions.setHeadless(true);
				chromeOptions.addArguments("--test-type");
				chromeOptions.addArguments("--start-maximized");
				chromeOptions.addArguments("--disable-infobars");
				chromeOptions.addArguments("--disable-extensions");
				chromeOptions.addArguments("--disable-gpu");
				chromeOptions.addArguments("--no-default-browser-check");
				chromeOptions.addArguments("--ignore-certificate-errors");
				chromeOptions.addArguments("--proxy-server='direct://'");
				chromeOptions.addArguments("--proxy-bypass-list=*");
				chromeOptions.setExperimentalOption("useAutomationExtension", false);
			
				desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				driver = new ChromeDriver(chromeOptions);

           }
			
		}
		

	// Set Test URL based on config.properties file.
	
	public static void setURL() {
		
			driver.manage().window().maximize();
			driver.get(url);
		
		}
		
	// Set Promotional Test URL based on config.properties file.
	
	public static void setPromotionalURL() {
		
		driver.manage().window().maximize();
		driver.get(promotionalurl);
	
	}
	
	
	// Set Direct Test URL based on config.properties file.
	
	public static void setDirectURL() {
		
		driver.manage().window().maximize();
		driver.get(directurl);
	
	}
	
	// Close the driver after running test script.
	
	public static void tearDownTest() {
		
			wait = new WebDriverWait(driver,3);
			driver.close();
			//driver.quit();
					
		}


}
