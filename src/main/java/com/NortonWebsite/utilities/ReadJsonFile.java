package com.NortonWebsite.utilities;

import java.io.FileReader;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;



public class ReadJsonFile {
	
	
	// Read Json test data from testData.json file with the parameters and values.
	
	public JsonObject readJason() {
		
			JsonObject rootObject = null;
		
		try
		{
		
			JsonParser parser = new JsonParser();
			JsonReader jReader = new JsonReader(new FileReader("./src/test/resources/testData.json"));
			jReader.setLenient(true);
			//FileReader jsonReader = new FileReader("./src/test/resources/testData.json");
			JsonElement rootElement = parser.parse(jReader);
			rootObject = rootElement.getAsJsonObject();
		
		}
		
		catch(Exception e)
		
		{
			e.printStackTrace();
		}
		
		return rootObject;
		
	}
		
}
