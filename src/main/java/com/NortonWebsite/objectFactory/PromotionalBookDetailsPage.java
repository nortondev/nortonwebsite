package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

public class PromotionalBookDetailsPage {
	
WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'panel panel-success')]//strong[1]")
	WebElement BookPrice;
	
	
	
	@FindBy(how = How.XPATH, using = "//div[@id='SearchQueryResult']//div[1]//div[2]//div[1]//div[1]//a[1]")
	WebElement FirstBookLink; //*[@id=\"SearchQueryResult\"]/div[3]/div[3]/div[1]/div[2]/div[1]/div[1]/a/span        
	
	
	public PromotionalBookDetailsPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@Step("Get Book Price on Book Details page,  Method: {method} ")
	public String getBookPrice() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(BookPrice));
		
		return BookPrice.getText();
		
	}
	
	
	@Step("Navigate to Book Details page,  Method: {method} ")
	public void clickFirstBookLink() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(FirstBookLink));
		
		FirstBookLink.click();
		
	}

}
