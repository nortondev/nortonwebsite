package com.NortonWebsite.objectFactory;

import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import ru.yandex.qatools.allure.annotations.Step;


public class SearchBook {
	
	WebDriver driver;
	
	
	// Finding Web Elements on Norton Home page with respect to Search using PageFactory.
	
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='SEARCH']")
	WebElement searchbox;
	
	@FindBy(how = How.XPATH, using = "//button[@class='searchButton']")
	WebElement searchicon;
	
	
	// Initializing Web Driver and PageFactory.
	
	public SearchBook(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();
	
	
	// Send Search keys to Search textbox.
	
	String searchText;
	String promoSearchText;
	
	@Step("Enter Search Text,  Method: {method} ")
	public void setSearchText1(String strSearchText) throws JsonIOException, JsonParseException, FileNotFoundException, InterruptedException {
		 
		 searchbox.sendKeys(jsonObj.getAsJsonObject("SearchText1").get("InputText").getAsString());
		 searchText = jsonObj.getAsJsonObject(strSearchText).get("InputText").getAsString();

	}		
	 
	@Step("Enter Search Text,  Method: {method} ")
	 public void setSearchText2(String strSearchText) throws JsonIOException, JsonParseException, FileNotFoundException, InterruptedException {
		 
		 searchbox.sendKeys(jsonObj.getAsJsonObject("SearchText2").get("InputText").getAsString());
		 searchText = jsonObj.getAsJsonObject(strSearchText).get("InputText").getAsString();
			
	}

	
	@Step("Enter Promo Search Text,  Method: {method} ")
	 public void setPromoSearch(String strPromoSearchText) throws JsonIOException, JsonParseException, FileNotFoundException, InterruptedException {
		 
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(searchbox));
		 searchbox.sendKeys(jsonObj.getAsJsonObject("PromoSearchText").get("InputPromoText").getAsString());
		 promoSearchText = jsonObj.getAsJsonObject(strPromoSearchText).get("InputPromoText").getAsString();
		 
	}
	 
	 // Capture and return Search key in Search textbox as input from Jason.
	 
	@Step("Return Search Text,  Method: {method} ")
	 public String getSearchText() throws InterruptedException
	 {
		Thread.sleep(3000);
		 return searchText;
	 }

	
	 // Click on Search Icon
	 
	@Step("Click Search Icon,  Method: {method} ")
	 public void clickSearchIcon() throws InterruptedException{
		 
		 searchicon.click();

    } 

}
