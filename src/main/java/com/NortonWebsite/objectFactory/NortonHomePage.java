package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Step;


public class NortonHomePage {
	
	WebDriver driver;
	
	// Finding Web Elements on Norton Home page with respect to Shop Book and Shop TextBooks using PageFactory.
	
	@FindBy(how = How.XPATH, using = "//button[text()='Shop Books']")
	WebElement shopBooks; //*[@id=\"homepage\"]/div[2]/div/div[2]/div/div/div[2]/div/div/div[1]/div/button 
	
	@FindBy(how = How.XPATH, using = "//button[text()='Shop Textbooks']")
	WebElement shopTextBooks; //*[@id=\"homepage\"]/div[2]/div/div[2]/div/div/div[2]/div/div/div[2]/div/button
	
	@FindBy(how = How.XPATH, using = "//img[@src='/static/assets/images/seagull.svg']")
	WebElement homeImage;
	
	@FindBy(how = How.XPATH, using = "//button[@class='link userlogin']")
	WebElement myAccount;
	
	@FindBy(how = How.XPATH, using = "//button[@class='link cartitems']")
	WebElement cartItems;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"loginEmail\"]")
	WebElement logInEmail;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"loginPassword\"]")
	WebElement logInPassword;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"LoginSubmitButton\"]")
	WebElement logInButton;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"dashboardMenu\"]/div[2]/ul/li[1]/div[1]/a")
	WebElement profile;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"profilePanel\"]/div[2]/div/div[2]/div[2]/div")
	WebElement loggedInUser;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"dashboardMenu--heading\"]/div/a/div/div[2]")
	WebElement logOut;
	
	@FindBy(how = How.XPATH, using = "//button[@class='button shop-books center-block']")
	WebElement buttonShopBook;
	
	
	
	// Initializing Web Driver and PageFactory.
	
	public NortonHomePage(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();
	
	
	@Step("Click on Shop Book button on Home Page,  Method: {method} ")
	public void clickShopBooks() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(shopBooks));
		
		Actions act = new Actions(driver);
		act.moveToElement(shopBooks).click().build().perform();
		
		//shopBooks.click();
		
	}
	
	@Step("Click on Shop TextBook button on Home Page,  Method: {method} ")
	public void clickShopTextBooks() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(shopTextBooks));
		
		Actions act = new Actions(driver);
		act.moveToElement(shopTextBooks).click().build().perform();
		
		//shopTextBooks.click();
		
	}
	
	@Step("Click on Seagull Image icon on Home Page,  Method: {method} ")
	public void clickHomeImage() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(homeImage));
		homeImage.click();
			
	}
	
	
	@Step("Click on Account Link on Home Page,  Method: {method} ")
	public void clickMyAccount() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(myAccount));
		myAccount.click();
		
	}
	
	
	@Step("Login to My Account,  Method: {method} ")
	public void loginMyAccount() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(logInEmail));
		logInEmail.sendKeys(jsonObj.getAsJsonObject("ExistingUser").get("loginemail").getAsString());
		
		wait.until(ExpectedConditions.visibilityOf(logInPassword));
		logInPassword.sendKeys(jsonObj.getAsJsonObject("ExistingUser").get("loginpassword").getAsString());
		
		wait.until(ExpectedConditions.visibilityOf(logInButton));
		logInButton.click();
		
	}

	
	@Step("Click on Profile Link on My Account,  Method: {method} ")
	public void clickProfile() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(profile));
		profile.click();
		
	}
	
	
	@Step("Validate Existing User logged in,  Method: {method} ")
	public String validateLoginUser() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(loggedInUser));
		
		return loggedInUser.getText();
		
	}
	
	
	@Step("Validate Norton Website Logout,  Method: {method} ")
	public void logOutWebsite() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(logOut));
		logOut.click();
		
	}
	
	
	@Step("Click on Cart Items in Header section,  Method: {method} ")
	public void clickCartItemsLink() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(cartItems));
		cartItems.click();
		
	}
	
	
	@Step("Click on ShopBook button on Checkout section,  Method: {method} ")
	public void clickShopBookButton() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(buttonShopBook));
		buttonShopBook.click();
		
	}
	

}
