package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Step;



public class BookSearchResultsPage {
	
WebDriver driver;
	
	
	// Finding Web Elements on Book Search Results page using PageFactory.
	

	@FindBy(how = How.XPATH, using = "//body//h1[1]")
	WebElement searchResultsText;
	
	@FindBy (how = How.XPATH, using = "//button[contains(text(),'Show More')]")
	WebElement showMoreText;
	
	@FindBy (how = How.XPATH, using = "//div[@class=\"ResultTitle\"]/a[1]")
	WebElement firstBook;
	
	
	// Initializing Web Driver and PageFactory.
	
	public BookSearchResultsPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();
	
	
	// Capture Search key on Search Results page.
	
	@Step("Get Search Key,  Method: {method} ")
	public String getSearchResultsText() throws Exception {
		
		//WebDriverWait wait = new WebDriverWait(driver,3);
		//TimeUnit.SECONDS.sleep(3);
		//wait.until(ExpectedConditions.visibilityOf(searchResultsText));
		Thread.sleep(3000);
		return searchResultsText.getText();
		
	}
	
	
	// Validate Show More button exist at the bottom of the search result list and click on it if exist.
	
	@Step("Validate Show More exist?,  Method: {method} ")
	public void showMoreExist() throws Exception {

		try {
			
			Thread.sleep(5000);
			if(showMoreText.isDisplayed()) {
				
				WebDriverWait wait = new WebDriverWait(driver,5);
				TimeUnit.SECONDS.sleep(5);
				
				wait.until(ExpectedConditions.visibilityOf(showMoreText));
				showMoreText.click();
				
				Thread.sleep(3000);
				
			}
	}
	
		catch (NoSuchElementException e) {
		
			throw new Exception ("Show More button is NOT displayed");
			
			}
	}
	
	
	//Click on first book link in Search result list.
		
	@Step("Select Book from Search Results,  Method: {method} ")
	public void clickFirstBook() throws Exception {
		
	WebDriverWait wait = new WebDriverWait (driver, 3);
	TimeUnit.SECONDS.sleep(3);
		
	wait.until(ExpectedConditions.visibilityOf(firstBook));
	firstBook.click();
					
	}
	
	
	@Step("Select a Book from Search Results list,  Method: {method} ")
	public String selectBook() throws Exception {

			WebElement ResultTitleBook;
			String BookName = null;
			
			for (int i=1; i<=30; i++) 
			{
				Thread.sleep(1000);
				ResultTitleBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				BookName = ResultTitleBook.getText();
				String JsonBookName = jsonObj.getAsJsonObject("ShopBookName").get("InputBook1").getAsString();
				
				if(BookName.contains(JsonBookName)) 
					{
						ResultTitleBook.click();
						break;
							
					}
		
			}
			
			return BookName;
			
	}
	
	@Step("Select a Text Book from Search Results list,  Method: {method} ")
	public String selectTextBook() throws Exception {

			WebElement ResultTitleTextBook;
			String TextBookName = null;
			
			for (int i=1; i<=30; i++) 
			{	
				Thread.sleep(1000);
				ResultTitleTextBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				
				TextBookName = ResultTitleTextBook.getText();
				String JsonBookName = jsonObj.getAsJsonObject("ShopTextBookName").get("InputBook2").getAsString();
				
				if(TextBookName.contains(JsonBookName)) 
					{
						ResultTitleTextBook.click();
						break;
							
					}
		
			}
			
			return TextBookName;
			
	}
	
	
	@Step("Select a Not Yet Published Book from Search Results list,  Method: {method} ")
	public String selectNYPBook() throws Exception {

			WebElement ResultTitleNYPBook;
			String NYPBookName = null;
			
			for (int i=1; i<=30; i++) 
			{
				Thread.sleep(1000);
				ResultTitleNYPBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				
				NYPBookName = ResultTitleNYPBook.getText();
				String JsonNYPBookName = jsonObj.getAsJsonObject("ShopNYPBook").get("InputBook3").getAsString();
				
				if(NYPBookName.contains(JsonNYPBookName)) 
					{
						ResultTitleNYPBook.click();
						break;
							
					}
		
			}
			
			return NYPBookName;
			
	}
	
	
	@Step("Select a Not Yet Published eBook from Search Results list,  Method: {method} ")
	public String selectNYPeBook() throws Exception {

			WebElement ResultTitleNYPeBook;
			String NYPeBookName = null;
			
			for (int i=1; i<=30; i++) 
			{
				Thread.sleep(1000);
				ResultTitleNYPeBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				
				NYPeBookName = ResultTitleNYPeBook.getText();
				String JsonNYPeBookName = jsonObj.getAsJsonObject("ShopNYPeBook").get("InputBook4").getAsString();
				
				if(NYPeBookName.contains(JsonNYPeBookName)) 
					{
						ResultTitleNYPeBook.click();
						break;
							
					}
		
			}
			
			return NYPeBookName;
			
	}
	
	
	@Step("Select a eBook Item from Search Results list,  Method: {method} ")
	public String selecteItemBook() throws Exception {

			WebElement ResultTitleeItemBook;
			String eItemBookName = null;
			
			for (int i=1; i<=30; i++) 
			{
				Thread.sleep(1000);
				ResultTitleeItemBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				
				eItemBookName = ResultTitleeItemBook.getText();
				String JsoneItemBookName = jsonObj.getAsJsonObject("ShopeItemBook").get("InputBook5").getAsString();
				
				if(eItemBookName.contains(JsoneItemBookName)) 
					{
					
						ResultTitleeItemBook.click();
						break;
							
					}
		
			}
			
			return eItemBookName;
			
	}

}
