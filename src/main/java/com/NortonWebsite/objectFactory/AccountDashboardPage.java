package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Step;

public class AccountDashboardPage {

	WebDriver driver;
	
	// Finding Web Elements on Norton Home page with respect to Shop Book and Shop TextBooks using PageFactory.
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"profilePanel\"]/div[2]/div/div/div[2]/div[3]")
	WebElement addressOne;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"profilePanel\"]/div[2]/div/div/div[3]/div[3]")
	WebElement addressTwo;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"profilePanel\"]/div[2]/div/div/div[3]/div[2]/div")
	WebElement addressTwoUpdate;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Save')]")
	WebElement addressTwoSave;
	
	
	// Initializing Web Driver and PageFactory.
	
	public AccountDashboardPage(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();
	
	
	@Step("Get Shipping Address One Information,  Method: {method} ")
	public String getAddressOne() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(addressOne));
		String AddressOne = addressOne.getText();
		return AddressOne;
		
	}
	
	
	@Step("Get Shipping Address Two Information,  Method: {method} ")
	public String getAddressTwo() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(addressTwo));
		String AddressTwo = addressTwo.getText();
		return AddressTwo;
		
	}
	
	
	@Step("Get Shipping Address from JSON,  Method: {method} ")
	public String getJSONAddress() throws Exception {
		
		String ShippingAddress = jsonObj.getAsJsonObject("ShippingAddress").get("address").getAsString();
		return ShippingAddress;
		
	}
	
	
	@Step("Validate multiple shipping addresses exist,  Method: {method} ")
	public boolean multipleAddresses() throws Exception {
		
		boolean AddressOneExist;
		boolean AddressTwoExist;
		
		try 
		{
			Thread.sleep(3000);
			AddressOneExist = addressOne.isDisplayed();
			AddressTwoExist = addressTwo.isDisplayed();
			
			if(AddressOneExist && AddressTwoExist)
			{
				
				return true;
				
			}
			
		}
		
		catch (NoSuchElementException e) {
			
			throw new NoSuchElementException("Error: No Shipping Address exist " + e.getMessage());
			
		}
		
		return false;
		
	}
	
	
	@Step("Update the Address Two,  Method: {method} ")
	public void editAddressTwo() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(addressTwoUpdate));
		addressTwoUpdate.click();
		
		wait.until(ExpectedConditions.visibilityOf(addressTwoSave));
		addressTwoSave.click();
		
	}

}
