package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Step;

public class RequestExamCopy {
	
	WebDriver driver;
	
	//Finding Elements with Request Exam Copy workflow.
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"for-instructors\"]/div[2]/div/div/div/div/div[2]")
	WebElement panelRequestExam;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Request Exam Copy']")
	WebElement buttonRequestExamCopy;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"noanim-tab-example-pane-1\"]/div/div[1]/div/div/input")
	WebElement schoolCity;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"noanim-tab-example-pane-1\"]/div/div[1]/div/ul/li[1]/a/div")
	WebElement selectCity;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"txtCollegeName\"]/div/div[2]")
	WebElement schoolDropDown;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"react-select-3-option-0\"]")
	WebElement selectSchool;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	WebElement buttonContinue;
	
	@FindBy(how = How.XPATH, using = "//input[@id='loginEmail']")
	WebElement loginEmail;
	
	@FindBy(how = How.XPATH, using = "//input[@id='loginPassword']")
	WebElement loginPassword;
	
	@FindBy(how = How.XPATH, using = "//button[@id='LoginSubmitButton']")
	WebElement loginButton;
	
	@FindBy(how = How.XPATH, using = "//input[@id='txtExamCourseName']")
	WebElement courseName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='txtExamCurrentBook']")
	WebElement currentBookName;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"dropdown-basic-default\"]")
	WebElement termDropDown;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"dropdown-basic-default\"]")
	WebElement yearDropDown;

	
	// Initializing Web Driver and PageFactory.
	
	public RequestExamCopy(WebDriver driver){
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file.
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();
	
	
	@Step("Click Request Exam Copy button if Request Exam panel exist, Method: {method} ")
	public void clickRequestExamCopy(String locator) throws Exception {
		
		boolean requestExamCopyPanelExist = true;
		
		try 
		{	
			//Thread.sleep(3000);
			requestExamCopyPanelExist = panelRequestExam.isDisplayed();
			
		
			if(requestExamCopyPanelExist == true)
			{
				Thread.sleep(3000);
				buttonRequestExamCopy.click();
			}
		
	    } catch (NullPointerException | NoSuchElementException e) {
	        System.err.println("Unable to locate Request Exam panel '" + locator + "'");
	        
	    } catch (Exception e) {
	        System.err.println("Unable to check display status of locator '" + locator + "'");
	        e.printStackTrace();
	    }
	
	}
	
	
	@Step("Enter login Information in Request Exam Copy flow, Method: {method} ")
	public void loginExamCopy() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(loginEmail));
		loginEmail.sendKeys(jsonObj.getAsJsonObject("AccountDetails").get("username").getAsString());
		
		wait.until(ExpectedConditions.visibilityOf(loginPassword));
		loginPassword.sendKeys(jsonObj.getAsJsonObject("AccountDetails").get("pswd").getAsString());
		
		wait.until(ExpectedConditions.visibilityOf(loginButton));
		loginButton.click();
		
	}

	
	@Step("Enter School Information, Method: {method} ")
	public void enterSchoolInformation() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(schoolCity));
		schoolCity.click();
		schoolCity.sendKeys(jsonObj.getAsJsonObject("StudentCity").get("EnterCity").getAsString());
		
		wait.until(ExpectedConditions.visibilityOf(selectCity));
		selectCity.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(schoolDropDown));
		schoolDropDown.click();
		
		wait.until(ExpectedConditions.visibilityOf(selectSchool));
		selectSchool.click();
		
		wait.until(ExpectedConditions.visibilityOf(buttonContinue));
		buttonContinue.click();
		
		Thread.sleep(3000);
		buttonContinue.click();
		
	}
	
	
	@Step("Enter Course Information, Method: {method} ")
	public void enterCourseInformation() throws Exception {
		
		WebElement termOption;
		String termLabel;
		String strTerm;
		
		WebElement yearOption;
		String yearLabel;
		String strYear;
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(courseName));
		courseName.sendKeys(jsonObj.getAsJsonObject("CourseInformation").get("CourseName").getAsString());
		
		wait.until(ExpectedConditions.visibilityOf(currentBookName));
		currentBookName.sendKeys(jsonObj.getAsJsonObject("CourseInformation").get("CurrentBookName").getAsString());
		
		wait.until(ExpectedConditions.visibilityOf(termDropDown));
		termDropDown.click();
		 
		 for(int i=1; i <5; i++) 
		 {  
			 termOption = driver.findElement(By.xpath("//*[@id=\"termAndYearRow\"]/div[1]/div/div/div/ul/li[" + i + "]"));
			 termLabel = termOption.getText();
			 strTerm = jsonObj.getAsJsonObject("CourseInformation").get("Term").getAsString();
			 
			 if(termLabel.equals(strTerm))
			 {
				 termOption.click();
				 break;
			 }
			 
		 }
		 
		
		 wait.until(ExpectedConditions.visibilityOf(yearDropDown));
		 yearDropDown.click();
			 
			 for(int i=1; i <6; i++) 
			 {  
				 yearOption = driver.findElement(By.xpath("//*[@id=\"termAndYearRow\"]/div[2]/div/div/div/ul/li[" + i + "]"));
				 yearLabel = yearOption.getText();
				 strYear = jsonObj.getAsJsonObject("CourseInformation").get("Year").getAsString();
				 
				 if(yearLabel.equals(strYear))
				 {
					 yearOption.click();
					 break;
				 }
				 
			 }
	}
	
}
