package com.NortonWebsite.objectFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import ru.yandex.qatools.allure.annotations.Step;

public class PromotionalAddToCartPage {
	
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//div[@class='cartbookprice']")
	WebElement CartDiscountedPrice;
	
	@FindBy(how = How.XPATH, using = "//div[@class='cartdiscountprice']")
	WebElement CartOriginalPrice;
	
	@FindBy(how = How.XPATH, using = "//span[@class='promoCheckMarkText']")
	WebElement PromoText;
	
	@FindBy(how = How.XPATH, using = "//div[@class='cartTotalArea']//div[2]//div[1]//div[2]")
	WebElement SubTotal;
	
	@FindBy(how = How.XPATH, using = "//span[@class='itemCount topMargin']")
	WebElement Total;
	
	@FindBy(how = How.XPATH, using = "//div[@class='cartTotalArea']//div[3]//div[1]//div[2]")
	WebElement EstimatedTax;
	
	@FindBy(how = How.XPATH, using = "//div[@class='subtotal fontWeight']")
	WebElement PromoCode;
	
	@FindBy(how = How.XPATH, using = "//img[@src='/static/assets/images/close.svg']")
	WebElement closePromoCode;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter Promo Code']")
	WebElement PromoCodeBox;
	
	@FindBy(how = How.XPATH, using = "//button[@id='buttonsend']")
	WebElement ApplyPromo;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[1]/span/label")
	WebElement FreeShippingOption;
	
	@FindBy(how = How.XPATH, using = "//button[@id='ProfileAddressSubmitButton']")
	WebElement SubmitShippingMethod;
	
	
	
	public PromotionalAddToCartPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();
	
	
	// Capture and return PromoCode as input from Jason.
	
	@Step("Return PromoCode from JSON file,  Method: {method} ")
	public String getCode1() throws IOException, JsonIOException, JsonParseException, FileNotFoundException, InterruptedException {
		
		String CodeText1 = jsonObj.getAsJsonObject("PromoCode1").get("InputCode1").getAsString();
		return CodeText1;
		
	}
	
	
	@Step("Return PromoCode from JSON file,  Method: {method} ")
	public String getCode2() throws IOException, JsonIOException, JsonParseException, FileNotFoundException, InterruptedException {
		
		String CodeText2 = jsonObj.getAsJsonObject("PromoCode2").get("InputCode2").getAsString();
		return CodeText2;
		
	}
	
	
	@Step("Get Book Price as Cart Original price,  Method: {method} ")
	public String getCartOriginalPrice() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(CartOriginalPrice));
		
		return CartOriginalPrice.getText();
		
	}
	
	
	@Step("Get Book Price as Cart Original price,  Method: {method} ")
	public String getCartDiscountedPrice() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(CartDiscountedPrice));
		
		return CartDiscountedPrice.getText();
		
	}

	
	@Step("Get SubTotal as Original price,  Method: {method} ")
	public String getSubTotal() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(SubTotal));
		
		return SubTotal.getText();
		
	}
	
	
	@Step("Verify Estimated tax is displayed as ZERO,  Method: {method} ")
	public boolean getEstimatedTax() throws Exception {
		
		Thread.sleep(3000);
		
		if(EstimatedTax.getText().equalsIgnoreCase("$0.00")) {
		
			return true;
	
		}
		
			return false;
	}
	
	
	@Step("Apply Promo Code SPRING193, Method: {method} ")
	public void applyPromoCode1() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(PromoCodeBox));
		PromoCodeBox.sendKeys(jsonObj.getAsJsonObject("PromoCode1").get("InputCode1").getAsString());
		ApplyPromo.click();
		
	}
	
	
	@Step("Apply Promo Code BERGIN, Method: {method} ")
	public void applyPromoCode2() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(PromoCodeBox));
		PromoCodeBox.sendKeys(jsonObj.getAsJsonObject("PromoCode2").get("InputCode2").getAsString());
		ApplyPromo.click();
		
	}
	
	
	@Step("Get PromoCode SPRING193 displayed in the Cart,  Method: {method} ")
	public String getPromoCode() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(PromoCode));
		
		Thread.sleep(3000);
		return PromoCode.getText();
		
	}
	
	
	@Step("Remove Promo Code from the Cart, Method: {method} ")
	public void removePromoCode() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(closePromoCode));
		closePromoCode.click();
		
	}
	
	
	@Step("Validate Free Shipping tax option is displayed,  Method: {method} ")
	public boolean getFreeShippingOption() throws Exception {
		
		Thread.sleep(3000);
		
		if(FreeShippingOption.getText().equalsIgnoreCase("Standard Shipping (Free)")) {
		
			return true;
	
		}
		
			return false;
	}
	
	
	@Step("Click on Free Shipping tax option,  Method: {method} ")
	
	public void clickFreeShippingOption() throws Exception {
		
		FreeShippingOption.click();
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(SubmitShippingMethod));
		SubmitShippingMethod.click();
		
	}

	
	
	@Step("Validate Promo Text is displayed,  Method: {method} ")
	public boolean promoText() throws Exception {
		
		boolean promoCodeExist = false;
		
		try {
			
			Thread.sleep(3000);
			promoCodeExist = PromoText.isDisplayed();
		}
		
		catch (NoSuchElementException e) {
			
			promoCodeExist = false;
		}
		
		catch (Exception e) {
			
			e.printStackTrace();
		}
		
		Thread.sleep(3000);
		if(promoCodeExist) {
			
			return true;
			
		}
		
		else {
			
			return false;
			
		}
			
	}
	
	
	@Step("Validate Promo Code Box is displayed,  Method: {method} ")
	public boolean promoCodeBox() throws Exception {
		
		boolean promoCodeBoxExist = true;
		
		try {
			
			Thread.sleep(3000);
			promoCodeBoxExist = PromoCodeBox.isDisplayed();
		}
		
		catch (NoSuchElementException e) {
			
			promoCodeBoxExist = true;
		}
		
		catch (Exception e) {
			
			e.printStackTrace();
		}
		
		Thread.sleep(3000);
		if(promoCodeBoxExist) {
			
			return true;
			
		}
		
		else {
			
			return false;
			
		}
		
	}

}
