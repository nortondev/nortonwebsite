package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Step;

public class AddToCart {
	
	WebDriver driver;
	
	
	// Finding Web Elements on Book Details page with respect to Add to Cart using PageFactory.
	
	
	@FindBy(how = How.XPATH, using = "//button[@id='CheckoutButton']")
	WebElement checkoutButton;  //div[@class='stickfooter']//div[2]//div[1]//div[2]//button[1]
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Email']")
	WebElement userName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='loginPassword']")
	WebElement password;
	
	@FindBy(how = How.XPATH, using = "//button[@id='LoginSubmitButton']")
	WebElement logInButton;
	
	@FindBy(how = How.CSS, using = "#creatAccountEmail")
	WebElement createAccountEmail;
	
	@FindBy(how = How.CSS, using = "#CreateAccountSubmitButton")
	WebElement createAccountButton;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountFirstName']")
	WebElement createAccountfName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountLastName']")
	WebElement createAccountlName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountReTypeEmail']")
	WebElement reTypeEmail;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountPassword']")
	WebElement Password;

	@FindBy(how = How.XPATH, using = "//button[@id='LoginSubmitButton']")
	WebElement loginButton;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	WebElement continueButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='text-center float-left ModalWizardHeader']")
	WebElement accountConfirmation;
//
//	@FindBy(how = How.XPATH, using = "//img[@src='/static/assets/images/close.svg']")
//	WebElement closeIcon;
//	
//	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[2]/header[1]/div[1]/div[2]/div[1]/button[1]/*")
//	WebElement cartIcon;
	
	
	// Initializing Web Driver and PageFactory.
	
	public AddToCart(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();

	
	// Navigate to Checkout page as New/Existing user.
	
	
	@Step("Click on Checkout,  Method: {method} ")
	public void clickCheckout() throws Exception {
		
		//WebDriverWait wait = new WebDriverWait(driver,5);
		//TimeUnit.SECONDS.sleep(5);
		
		//wait.until(ExpectedConditions.visibilityOf(checkoutButton));
		
		Thread.sleep(5000);
		checkoutButton.click();
		
	}
	
	@Step("Login with an Existing user,  Method: {method} ")
	public void accountLogin() throws Exception {
		
		//WebDriverWait wait = new WebDriverWait(driver,3);
		//TimeUnit.SECONDS.sleep(3);
		
		//wait.until(ExpectedConditions.visibilityOf(userName));
		Thread.sleep(3000);
		userName.sendKeys(jsonObj.getAsJsonObject("AccountDetails").get("username").getAsString());
		
		//wait.until(ExpectedConditions.visibilityOf(password));
		Thread.sleep(3000);
		password.sendKeys(jsonObj.getAsJsonObject("AccountDetails").get("pswd").getAsString());
		
		Thread.sleep(3000);
		logInButton.click();
		
	}
	

	@Step("Create a New Login Account,  Method: {method} ")
	public String createAccount() throws Exception {
		
		Thread.sleep(3000);
		createAccountEmail.sendKeys(jsonObj.getAsJsonObject("NewAccount").get("email").getAsString());
		
		Thread.sleep(3000);
		createAccountButton.click();
		
		Thread.sleep(3000);
		createAccountfName.sendKeys(jsonObj.getAsJsonObject("NewAccount").get("fname").getAsString());
		
		Thread.sleep(3000);
		createAccountlName.sendKeys(jsonObj.getAsJsonObject("NewAccount").get("lname").getAsString());
		
		Thread.sleep(3000);
		reTypeEmail.sendKeys(jsonObj.getAsJsonObject("NewAccount").get("retypeemail").getAsString());
		
		Thread.sleep(3000);
		Password.sendKeys(jsonObj.getAsJsonObject("NewAccount").get("password").getAsString());
		
		Thread.sleep(3000);
		loginButton.click();
		
		Thread.sleep(3000);
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(accountConfirmation));
		String confirmationText = accountConfirmation.getText();
		return confirmationText;
		
	}
	
	
	@Step("Create a New Login Account,  Method: {method} ")
	public String shippingAccount() throws Exception {
		
		Thread.sleep(3000);
		createAccountEmail.sendKeys(jsonObj.getAsJsonObject("ShippingAccount").get("email").getAsString());
		
		Thread.sleep(3000);
		createAccountButton.click();
		
		Thread.sleep(3000);
		createAccountfName.sendKeys(jsonObj.getAsJsonObject("ShippingAccount").get("fname").getAsString());
		
		Thread.sleep(3000);
		createAccountlName.sendKeys(jsonObj.getAsJsonObject("ShippingAccount").get("lname").getAsString());
		
		Thread.sleep(3000);
		reTypeEmail.sendKeys(jsonObj.getAsJsonObject("ShippingAccount").get("retypeemail").getAsString());
		
		Thread.sleep(3000);
		Password.sendKeys(jsonObj.getAsJsonObject("ShippingAccount").get("password").getAsString());
		
		Thread.sleep(3000);
		loginButton.click();
		
		Thread.sleep(3000);
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(accountConfirmation));
		String confirmationText = accountConfirmation.getText();
		return confirmationText;
		
	}
	
	
	@Step("Continue to Checkout,  Method: {method} ")
	public void continueCheckout() throws Exception {
		
		Thread.sleep(3000);
		continueButton.click();
		
	}
	
}
