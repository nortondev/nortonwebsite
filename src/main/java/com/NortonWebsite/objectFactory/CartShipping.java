package com.NortonWebsite.objectFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import ru.yandex.qatools.allure.annotations.Step;

public class CartShipping {
	
	WebDriver driver;
	
	
	// Finding Web Elements on Book Details page with respect to Add to Cart using PageFactory.
	
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"studentInformationPanel--heading\"]/div/a/div/div[1]")
	WebElement studentPanel;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"studentInformationPanel--body\"]/div/div/div/div[1]/label/input")
	WebElement radioButtonStudent;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"studentInformationPanel--body\"]/div/div/div/div[2]/label/input")
	WebElement radioButtonNotStudent;
	
	//div[@id='studentInformationPanel--body']//div[2]//label[1]
	
	@FindBy(how = How.XPATH, using = "//span[@class='PolicyTitle']//a[contains(text(),'international')]")
	WebElement policyLinkOne;
	
	@FindBy(how = How.XPATH, using = "//span[@class='PolicyTitle']//a[contains(text(),'high school orders')]")
	WebElement policyLinkTwo;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"refCollegeCityName\"]/div/div[1]")
	WebElement enterCity;  //*[@id=\"noanim-tab-example-pane-1\"]/div/div[1]/div/div/input           
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"refCollegeCityName\"]/div/div[1]/div/ul/li[1]")
	WebElement selectCity;
	
	@FindBy(how = How.XPATH, using = "//div[@class='css-bg1rzq-control']//div[@class='css-1hwfws3']")
	WebElement schoolDropDown;        
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"react-select-2-option-0\"]")
	WebElement selectSchool;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Save and Continue')]")
	WebElement savenContinueButton;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"firstName\"]")
	WebElement shippingFirstName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='lastName']")
	WebElement shippingLastName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='address1']")
	WebElement shippingAddress;
	
	@FindBy(how = How.XPATH, using = "//input[@id='city']")
	WebElement shippingCity;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"drpState\"]/div/div/div/div[2]/div[2]")
	WebElement shippingStateButton;
	//*[@class='css-19bqh2r']
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"react-select-2-option-36\"]")
	WebElement shippingState;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"drpState\"]/div/div/div/div[2]/div[1]")
	WebElement removeShippingState;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"zip\"]")
	WebElement shippingZip;
	
	@FindBy(how = How.XPATH, using = "//input[@id='phone']")
	WebElement shippingPhone;
	
	@FindBy(how = How.XPATH, using = "//label[@for='shippingMethods0']")
	WebElement upsgRadioOption;
	
	@FindBy(how = How.XPATH, using = "//button[@id='ProfileAddressSubmitButton']")
	WebElement submitButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='buttonlabel panelEdit']")
	WebElement editShippingButton;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/span[2]")
	WebElement shippingMethodErr;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[1]/span/label")
	WebElement freeShippingText;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div/div/div[2]/div/div/div[3]/div[1]/div[3]/div[4]/div/div[2]")
	WebElement shippingTaxValue;           
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[2]/span/label")
	WebElement UPShippingText;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div/div/div[2]/div/div/div[3]/div[1]/div[3]/div[4]/div/div[2]")
	WebElement UPSshippingTaxValue;  
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[1]/span/label/span")
	WebElement strikeOutShippingPrice;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[1]/span/label/b")
	WebElement discountedShippingPrice;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"zipCodeParent\"]/div/span")
	WebElement shippingZipError;
	
	
	// Initializing Web Driver and PageFactory.
	
	public CartShipping(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();

	
	// Select and Enter Shipping information in Checkout page.
	
	
	@Step("Return Shipping Prices from JSON file,  Method: {method} ")
	public String[] getShippingPrice() throws IOException, JsonIOException, JsonParseException, FileNotFoundException, InterruptedException {
		
		String[] JsonShippingPriceArray = new String[6];
		JsonShippingPriceArray[0] = jsonObj.getAsJsonObject("ShippingPrice").get("StrickeOutShippingPrice").getAsString();
		JsonShippingPriceArray[1] = jsonObj.getAsJsonObject("ShippingPrice").get("DiscountedShippingPrice").getAsString();
		JsonShippingPriceArray[2] = jsonObj.getAsJsonObject("ShippingPrice").get("NYUPSShippingPrice").getAsString();
		JsonShippingPriceArray[3] = jsonObj.getAsJsonObject("ShippingPrice").get("FLUPSShippingPrice").getAsString();
		JsonShippingPriceArray[4] = jsonObj.getAsJsonObject("ShippingPrice").get("QtyFLUPSShippingPrice").getAsString();
		JsonShippingPriceArray[5] = jsonObj.getAsJsonObject("ShippingPrice").get("UPSShippingPrice").getAsString();
		
		return JsonShippingPriceArray;
		
	}
	
	
	@Step("Select a Student option,  Method: {method} ")
	public void studentInfo() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		boolean studentPanelExist = false;

		try 
		{

			studentPanelExist = studentPanel.isDisplayed();

		}
		
		catch (NoSuchElementException e) 
		{

			studentPanelExist = false;
			
		}

		if(studentPanelExist) 
		{
		
			wait.until(ExpectedConditions.visibilityOf(radioButtonStudent));
			radioButtonStudent.click();
		
			wait.until(ExpectedConditions.visibilityOf(savenContinueButton));
			savenContinueButton.click();
		}
		
		else
		{
			
		}
		
	}
	
	
	@Step("Select Not a Student option,  Method: {method} ")
	public void notStudentInfo() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);	

		boolean studentPanelExist = false;

		try 
		{

			studentPanelExist = studentPanel.isDisplayed();

		}
		
		catch (NoSuchElementException e) 
		{

			studentPanelExist = false;
			
		}

		if(studentPanelExist) 
		{
		
			wait.until(ExpectedConditions.visibilityOf(radioButtonNotStudent));
			radioButtonNotStudent.click();
		
			wait.until(ExpectedConditions.visibilityOf(savenContinueButton));
			savenContinueButton.click();
		
		}
		
		else 
		{
			
		}
	}
	
	
	@Step("Verify the Policy links are exist,  Method: {method} ")
	public boolean policyLinks() throws Exception {
		
		boolean PolicyLinkOneExist;
		boolean PolicyLinkTwoExist;
		
		Thread.sleep(3000);
		
		try 
		{
			PolicyLinkOneExist = policyLinkOne.isDisplayed();
			PolicyLinkTwoExist = policyLinkTwo.isDisplayed();
			
			if(PolicyLinkOneExist && PolicyLinkTwoExist)
			{
				
				return true;
				
			}
			
		}
		
		catch (NoSuchElementException e) {
			
			throw new NoSuchElementException("Error: Policy Links are NOT displayed " + e.getMessage());
			
		}
		
		return false;
		
	}
	
	
	@Step("Student Information Exist,  Method: {method} ")
	public boolean studentInfoExist() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);	

		boolean studentPanelExist = false;

		try 
		{

			studentPanelExist = studentPanel.isDisplayed();

		}
		
		catch (NoSuchElementException e) 
		{

			studentPanelExist = false;
			//throw new Exception ("Student Panel is NOT displayed");
			
		}

		if(studentPanelExist) 
		{

			wait.until(ExpectedConditions.visibilityOf(radioButtonStudent));
			radioButtonStudent.click();
			
			//wait.until(ExpectedConditions.visibilityOf(enterCity));
			Thread.sleep(3000);
			enterCity.click();
			
			Thread.sleep(3000);
			enterCity.sendKeys(jsonObj.getAsJsonObject("StudentCity").get("EnterCity").getAsString());
			
			wait.until(ExpectedConditions.visibilityOf(selectCity));
			selectCity.click();
			
			wait.until(ExpectedConditions.visibilityOf(schoolDropDown));
			schoolDropDown.click();
			
			wait.until(ExpectedConditions.visibilityOf(selectSchool));
			selectSchool.click();
			
			//wait.until(ExpectedConditions.visibilityOf(savenContinueButton));
			Thread.sleep(3000);
			savenContinueButton.click();

		}
		
		return studentPanelExist;


	}
	
	
	@Step("Shipping Information Exist,  Method: {method} ")
	public void shippingInfo() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);	

		boolean studentPanelExist = false;

		try 
		{

			studentPanelExist = studentPanel.isDisplayed();

		}
		catch (NoSuchElementException e) 
		{

			studentPanelExist = false;
		}
		catch (Exception e) 
		{

			e.printStackTrace();
		}

		if(studentPanelExist) 
		{

			wait.until(ExpectedConditions.visibilityOf(radioButtonNotStudent));
			Thread.sleep(3000);
			radioButtonNotStudent.click();

			wait.until(ExpectedConditions.visibilityOf(savenContinueButton));
			savenContinueButton.click();
		
		}
		
		Thread.sleep(3000);
		String shippingName = shippingFirstName.getAttribute("value");
		
		if (shippingName.isEmpty() == false) 
		{ 

			//wait.until(ExpectedConditions.visibilityOf(radioButtonNotStudent));
			Thread.sleep(3000);
			
			wait.until(ExpectedConditions.visibilityOf(upsgRadioOption)); 
			upsgRadioOption.click();
		}
		
		else 
		{

			//wait.until(ExpectedConditions.visibilityOf(shippingFirstName));
			Thread.sleep(3000);
			shippingFirstName.sendKeys(jsonObj.getAsJsonObject("ShippingInformation").get("firstname").getAsString());

			//wait.until(ExpectedConditions.visibilityOf(shippingLastName));
			Thread.sleep(3000);
			shippingLastName.sendKeys(jsonObj.getAsJsonObject("ShippingInformation").get("lastname").getAsString());

			//wait.until(ExpectedConditions.visibilityOf(shippingAddress));
			Thread.sleep(3000);
			shippingAddress.sendKeys(jsonObj.getAsJsonObject("ShippingInformation").get("address").getAsString());

			//wait.until(ExpectedConditions.visibilityOf(shippingCity));
			Thread.sleep(3000);
			shippingCity.sendKeys(jsonObj.getAsJsonObject("ShippingInformation").get("city1").getAsString());

			shippingStateButton.click();

			Thread.sleep(3000);

			WebElement stateOption;
			String stateName;
			for(int i=0; i<51; i++)
			{
				//stateOption = driver.findElement(By.xpath("//*[@id=\"react-select-2-option-" + i + "\"]"));
				stateOption = driver.findElement(By.id("react-select-2-option-" + i));
				stateName = stateOption.getText();
				if(stateName.equals(jsonObj.getAsJsonObject("ShippingInformation").get("state1").getAsString()))
				{
					stateOption.click();
					break;
				}
			}						

			//wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zip = jsonObj.getAsJsonObject("ShippingInformation").get("zip1").getAsString();
			int zipAsInt = Integer.parseInt(zip);
			Thread.sleep(3000);
			shippingZip.sendKeys(String.valueOf(zipAsInt));

			Thread.sleep(3000);
			shippingPhone.click();

			// Read Json Array
			Thread.sleep(3000);
			JsonArray phoneArray = jsonObj.getAsJsonObject("ShippingInformation").getAsJsonArray("phone");

			for(int i=0; i<phoneArray.size(); i++) {

				String PhoneNumber = phoneArray.get(i).toString();
				shippingPhone.sendKeys(PhoneNumber);

			}
		}
	}

	
	@Step("Select Shipping Method,  Method: {method} ")
	public void shippingMethod() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		Thread.sleep(3000);
		
		if(upsgRadioOption.isSelected()) 
		{
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			submitButton.click();
		}
		
		else 
		{
			
			//wait.until(ExpectedConditions.visibilityOf(upsgRadioOption));
			Thread.sleep(3000);
			upsgRadioOption.click();
			
			//wait.until(ExpectedConditions.visibilityOf(submitButton));
			Thread.sleep(3000);
			submitButton.click();
		}
	
	}
	
		
		@Step("Submit Shipping Method,  Method: {method} ")
		public void submitShippingMethod() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			Thread.sleep(3000);
			submitButton.click();

		}


		@Step("Verify Shipping Method Error,  Method: {method} ")
		public String shippingMethodErr() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingMethodErr));
			
			return shippingMethodErr.getText();

		}
		
		
		@Step("Free Shipping option Eixst and Selected ?, Method: {method} ")
		public boolean[] freeShippingExist(String locator) throws Exception {
			
			boolean[] smValueArray = new boolean[2];
			
			try 
			{	
				Thread.sleep(3000);
				smValueArray[0] = freeShippingText.isDisplayed();
				
				Thread.sleep(3000);
				smValueArray[1] = freeShippingText.isSelected();
			
				if(smValueArray[0])
				{
					if(smValueArray[1])
					{
						smValueArray[1] = true;
					}
					
					else
					{
						smValueArray[1] = false;
						
					}
				}
			
		    } catch (NullPointerException | NoSuchElementException e) {
		        System.err.println("Unable to locate Free Shipping Method '" + locator + "'");
		        
		    } catch (Exception e) {
		        System.err.println("Unable to check display status of locator '" + locator + "'");
		        e.printStackTrace();
		    }
			
			return smValueArray;
			
		}
		
		
		@Step("Get the list of Shipping Methods,  Method: {method} ")
		public String[] shippingMethodList(String locator) throws Exception {
		
		String[] smTextArray = new String[5];
		
		try 
		{
			//Get all web elements into list					
			//List<WebElement> smList=driver.findElements(By.xpath("//*[@id=\"shippingCarrier\"]/div/div"));
			
			WebElement smLabel;
			
			for(int i=1; i<5; i++)
			{

				smLabel = driver.findElement(By.xpath("//*[@id=\"shippingCarrier\"]/div/div/div[" + i + "]/span/label"));
				smTextArray[i] = smLabel.getText();
			}
		}
		
		catch (IndexOutOfBoundsException e) {
			
			System.err.println("Unexpected Error occurred" + locator + "'");
			
		}
	    
		return smTextArray;
		
	}
		
		
		@Step("Verify Shipping Methods selected by default,  Method: {method} ")
		public boolean[] shippingMethodListSelected(String locator) throws Exception {
		
			boolean[] smBooleanArray = new boolean[4];
		
		try 
		{
			//Get all web elements into list					
			//List<WebElement> smList=driver.findElements(By.xpath("//*[@id=\"shippingCarrier\"]/div/div"));
			
			WebElement smLabel;
			
			int i;
			
			for(i=1; i<4; i++)
			{

				smLabel = driver.findElement(By.xpath("//*[@id=\"shippingCarrier\"]/div/div/div[" + i + "]/span/label"));
				smBooleanArray[i] = smLabel.isSelected();
			}
		}
		
			catch (IndexOutOfBoundsException e) {
			
				System.err.println("Unexpected Error occurred" + locator + "'");
			
			}
		
		return smBooleanArray;
		
	}
		
		
		@Step("Get Free Shipping Tax value,  Method: {method} ")
		public String getFreeShippingTaxValue() throws Exception {
			
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingTaxValue));
			
			return shippingTaxValue.getText();
			
		}
		
		
		@Step("Get UPS Shipping Tax value,  Method: {method} ")
		public String getUPSShippingTaxValue() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(UPShippingText));
			UPShippingText.click();
			
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			submitButton.click();
			
			//wait.until(ExpectedConditions.visibilityOf(UPSshippingTaxValue));
			Thread.sleep(3000);
			return UPSshippingTaxValue.getText();
			
		}
		
		
		@Step("Return Free Shipping Tax value when Free Shipping selected,  Method: {method} ")
		public String returnFreeShippingTaxValue() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(freeShippingText));
			freeShippingText.click();
			
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			submitButton.click();
			
			Thread.sleep(3000);
			return shippingTaxValue.getText();
			
		}
		
		
		@Step("Validate StrikeOut and Discounted Shipping prices,  Method: {method} ")
		public String[] getDiscountedShipping() throws Exception {
			
			boolean discountedShippingExist = false;
			boolean strikeOutShippingExist = false;
			
			String[] shippingPriceArray = new String[2];
			
			try 
			{
				strikeOutShippingExist = strikeOutShippingPrice.isDisplayed();
				discountedShippingExist = discountedShippingPrice.isDisplayed();

			}
			catch (NoSuchElementException e) 
			{

				strikeOutShippingExist = false; 
				discountedShippingExist = false;
				
			}
			catch (Exception e) 
			{

				e.printStackTrace();
			}

			if(strikeOutShippingExist && discountedShippingExist) 
			{
				WebDriverWait wait = new WebDriverWait(driver,3);
				TimeUnit.SECONDS.sleep(3);
				
				wait.until(ExpectedConditions.visibilityOf(strikeOutShippingPrice));
				shippingPriceArray[0] = strikeOutShippingPrice.getText();

				wait.until(ExpectedConditions.visibilityOf(discountedShippingPrice));
				shippingPriceArray[1] = discountedShippingPrice.getText();
		
			}
			
			return shippingPriceArray;
		}
		
		
		@Step("Update Shipping Information: {method} ")
		public void updateShipping() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(editShippingButton));
			editShippingButton.click();
			
		}
		
		
		@Step("Update Shipping State and Zip Code: {method} ")
		public void updateZipCode() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingCity));
			String shippingCityLen = shippingCity.getAttribute("value");
			int lengthText = shippingCityLen.length();

			for(int i = 0; i < lengthText; i++){
				shippingCity.sendKeys(Keys.ARROW_LEFT);
				shippingCity.sendKeys(Keys.DELETE);
			}
			
			shippingCity.sendKeys(jsonObj.getAsJsonObject("ShippingInformation").get("city2").getAsString());
			
			String JsonStateName;
			JsonStateName = jsonObj.getAsJsonObject("ShippingInformation").get("state2").getAsString();
			
//			wait.until(ExpectedConditions.visibilityOf(removeShippingState));
//			removeShippingState.click();
			
			Thread.sleep(3000);
			shippingStateButton.click();
			
			//stateOption = driver.findElements(By.className("css-fk865s-option"));
			
//			WebElement stateName;
//
//			for(int i=0; i <57; i++)
//			 { 
//				Thread.sleep(3000);
//				stateName = driver.findElement(By.id("react-select-2-option-" + i)); 
//				
//				 if(JsonStateName == stateName.getText()) 
//				 {
//					 stateName.click();
//					 break;
//				 }
//					 
//			 }
				
				//list of all State options
				List<WebElement> selectStateValues=driver.findElements(By.id("react-select-2-input"));
				//String[] noOfStateArray = new String selectStateValues.size();
				
				System.out.println(selectStateValues.size());
				
				for(int i=0; i <=selectStateValues.size(); i++)
				 { 
					WebElement stateValue = selectStateValues.get(i);
					System.out.println(stateValue);
				
					if(JsonStateName == stateValue.getText()) 
					{
						stateValue.click();
						break;				
					}

				 }
			
			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zipLen = shippingZip.getAttribute("value");
			int lenText = zipLen.length();

			for(int i = 0; i < lenText; i++){
				shippingZip.sendKeys(Keys.ARROW_LEFT);
				shippingZip.sendKeys(Keys.DELETE);
			}
			
			String zip = jsonObj.getAsJsonObject("ShippingInformation").get("zip2").getAsString();
			int zipAsInt = Integer.parseInt(zip);

			shippingZip.sendKeys(String.valueOf(zipAsInt));
			
		}
		
		
		@Step("Remove Shipping ZipCode,  Method: {method} ")
		public void removeShippingZipCode() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zipLen = shippingZip.getAttribute("value");
			int lenText = zipLen.length();

			for(int i = 0; i < lenText; i++){
				shippingZip.sendKeys(Keys.ARROW_LEFT);
				shippingZip.sendKeys(Keys.DELETE);
			}
		}
			
		@Step("Re-enter Shipping ZipCode2,  Method: {method} ")	
		public void enterShippingZipCode2() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zip = jsonObj.getAsJsonObject("ShippingInformation").get("zip2").getAsString();
			int zipAsInt = Integer.parseInt(zip);
			shippingZip.sendKeys(String.valueOf(zipAsInt));
			
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(shippingPhone));
			act.moveToElement(shippingPhone).click().build().perform();
			
		}
		
		
		@Step("Re-enter Shipping ZipCode,  Method: {method} ")	
		public String shippingZipCodeErr() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingZipError));
			return shippingZipError.getText();
			
		}
		
		@Step("Re-enter Shipping ZipCode1,  Method: {method} ")	
		public void enterShippingZipCode1() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zip = jsonObj.getAsJsonObject("ShippingInformation").get("zip1").getAsString();
			int zipAsInt = Integer.parseInt(zip);
			shippingZip.sendKeys(String.valueOf(zipAsInt));
			
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(shippingPhone));
			act.moveToElement(shippingPhone).click().build().perform();
			
		}
		
}
