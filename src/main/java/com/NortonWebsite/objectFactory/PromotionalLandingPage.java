package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

public class PromotionalLandingPage {
	
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[1]/div[2]/div[1]/div[6]/div/span[1]")
	WebElement DiscountPrice; //*[@id=\"SearchQueryResult\"]/div/div[3]/div[3]/div[1]/div[2]/div[1]/div[6]/div/span[1]        
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[1]/div[2]/div[1]/div[6]/div/span[2]")
	WebElement SellPrice; //*[@id=\"SearchQueryResult\"]/div/div[3]/div[3]/div[1]/div[2]/div[1]/div[6]/div/span[2]
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"buttonContainer\"]")
	WebElement PromotionalAddToCart;  //*[@id="buttonContainer"]
	
	
	public PromotionalLandingPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	@Step("Get Discount price against the book,  Method: {method} ")
	public String getDiscountPrice() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(DiscountPrice));
		
		return DiscountPrice.getText();
		
	}

	
	@Step("Get Sell price against the book,  Method: {method} ")
	public String getSellPrice() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(SellPrice));
		
		return SellPrice.getText();
		
	}
	
	
	@Step("Click on Add to cart against promotional book,  Method: {method} ")
	public void clickPromotionalAddtoCart() throws Exception {
		
//		WebDriverWait wait = new WebDriverWait(driver,3);
//		TimeUnit.SECONDS.sleep(3);
//		
//		wait.until(ExpectedConditions.visibilityOf(PromotionalAddToCart));
		
		Thread.sleep(3000);
		PromotionalAddToCart.click();
		
	}
	
}
