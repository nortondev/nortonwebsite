package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;

public class OrderConfirmationPage {
	
	WebDriver driver;
	
	//Get Order Details from the Order Confirmation page.
	
	@FindBy(how = How.XPATH, using = "//div[@class='OrderConfirm']")
	WebElement orderConfirm;
	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]")
	WebElement orderNumber;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div[9]")
	WebElement orderShippingAddress;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div[9]/div[2]")
	WebElement shippingAddressLine1;
	
	
	
	// Initializing Web Driver and PageFactory.
	
	public OrderConfirmationPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	
	}
	
	@Step("Get Order Details,  Method: {method} ")
	public void orderDetails() throws Exception {
		
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		//wait.until(ExpectedConditions.visibilityOf(orderPlacer));
		Thread.sleep(5000);
		String orderConfirmation = orderConfirm.getText();
		Assert.assertEquals(orderConfirmation, "Your Order Has Been Placed");
		
		wait.until(ExpectedConditions.visibilityOf(orderNumber));
		String orderNo = orderNumber.getText();
		System.out.println(orderNo);
		
	}
	
	
	@Step("Order Shipping Address Exist,  Method: {method} ")
	public boolean orderShippingAddress() throws Exception {
		
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(orderShippingAddress));
		boolean ShippingAddress = orderShippingAddress.isDisplayed();
		
		return ShippingAddress;

	}
	
	
	@Step("Get Shipping Address Line 1,  Method: {method} ")
	public String getShippingAddress() throws Exception {
		
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(shippingAddressLine1));
		String ShippingAddressLine1 = shippingAddressLine1.getText();
		
		return ShippingAddressLine1;

	}
	
}
