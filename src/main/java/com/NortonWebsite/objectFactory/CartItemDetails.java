package com.NortonWebsite.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonObject;

import ru.yandex.qatools.allure.annotations.Step;

public class CartItemDetails {
	
	WebDriver driver;
	
	
	// Finding Web Elements on Book Details page with respect to Add to Cart using PageFactory.
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Remove')]")
	WebElement removeButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='empty-text']")
	WebElement emptyCart;
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown btn-group btn-group-default']")
	WebElement quantityDropDown;
	
	@FindBy(how = How.XPATH, using = "//body//li[6]")
	WebElement selectQuantityValue;
	
	@FindBy(how = How.XPATH, using = "//body//li[2]")
	WebElement selectShippingQuantity;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title']")
	WebElement modleTitle;
	
	@FindBy(how = How.XPATH, using = "//button[@id='QTYMessageCloseButton']")
	WebElement qtyCloseButton;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"CommonModelPop\"]/div/div/div[2]/div[1]/div[1]/span")
	WebElement commercialText;        
	
	@FindBy(how = How.XPATH, using = "//div[@class='qtyMessageDetail']//div[2]")
	WebElement contactPhoneNumber; //div[@class='modal-body']//div[3]   
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-body']//div[3]")
	WebElement contactEmail; //div[@role='dialog']//div[4]
	
	
	// Initializing Web Driver and PageFactory.
	
	public CartItemDetails(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();

	
	// Validate Cart Item Details
	
	@Step("Remove the items from the Cart,  Method: {method} ")
	public void clickRemove() throws Exception {
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 3);
				TimeUnit.SECONDS.sleep(3);
				
				wait.until(ExpectedConditions.visibilityOf(removeButton));
				removeButton.click();
		}
		
		catch (NoSuchElementException e) {
			
			throw new Exception ("Remove button does not exist");
			
		}
		
	}
	
	@Step("Capture Empty Cart text, Method: {method} ")
	public String getEmptyCartText() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(emptyCart));
		
		return emptyCart.getText();
		
	}
	
	
	@Step("Select Quantity of Books,  Method: {method} ")
	public void selectQuantity() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(quantityDropDown));
		quantityDropDown.click();
		
		wait.until(ExpectedConditions.visibilityOf(selectQuantityValue));
		selectQuantityValue.click();
		
	}
	
	@Step("Select Shipping Quantity of Books,  Method: {method} ")
	public void selectShippingQuantity() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(quantityDropDown));
		quantityDropDown.click();
		
		wait.until(ExpectedConditions.visibilityOf(selectShippingQuantity));
		selectShippingQuantity.click();
		
	}
	
	
	@Step("Verify the language on PopUp,  Method: {method} ")
	public String[] qtyPopUpMessage() throws Exception {
		
		String[] strArray = new String[4];
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(modleTitle));
		strArray[0] = modleTitle.getText();
		
		wait.until(ExpectedConditions.visibilityOf(commercialText));
		strArray[1] = commercialText.getText();
		
		wait.until(ExpectedConditions.visibilityOf(contactPhoneNumber));
		strArray[2] = contactPhoneNumber.getText();
		
		wait.until(ExpectedConditions.visibilityOf(contactEmail));
		strArray[3] = contactEmail.getText();
		
		
		return strArray;
		
	}
	
	@Step("Checkout PopUp Close,  Method: {method} ")
	public boolean checkoutPopUpDisplay() throws Exception {
		
		boolean closeButtonExist;
		
		try 
		{
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(quantityDropDown));
			closeButtonExist = qtyCloseButton.isDisplayed();
			
		}
		
		catch (NoSuchElementException e) {
			
			throw new Exception ("Close button does not exist");
			
		}
			
		if(closeButtonExist)
		{
			qtyCloseButton.click();
		}
		
		return closeButtonExist;
		
	}
	
}

