package com.NortonWebsite.objectFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.NortonWebsite.utilities.ReadJsonFile;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import ru.yandex.qatools.allure.annotations.Step;



public class BookDetailsPage {
	
	WebDriver driver;
	
	// Finding Web Elements on Book Details page using PageFactory.
	
	@FindBy(how = How.XPATH, using = "//h1[@class='title']")
	WebElement bookTitle;
	
	@FindBy(how = How.XPATH, using = "//a[@class='ContributorfirstNameURL']")
	WebElement AuthorName;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'About the Book')]")
	WebElement AboutTheBook;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Product Details')]")
	WebElement ProductDetails;
	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[3]/div[1]/section[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/row[1]/ul[2]/li[2]")                               
	WebElement ISBN;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-heading-2\"]/div/a/div[1]/div[1]")
	WebElement bookBinder;
	
	//*[@id="avaliableFormatPanelGrp-heading-2"]/div/a/div[1]/div[1]
	//driver.findElement(By.linkText("Loose Leaf'"));
	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[3]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/button[1]")
	WebElement addtoCart1;            //*[@id="avaliableFormatPanelGrp-body-1"]/div/div[2]/div/button
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-body-2\"]/div/div[2]/div/button")
	WebElement addtoCart2;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-body-3\"]/div/div/div[1]/div/button")
	WebElement addtoCart3;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-heading-1\"]/div/a/div[1]/div[2]")
	WebElement NYPText; //div[@class='header-subcontent']
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-heading-1\"]/div/a/div[1]/div[1]")
	WebElement eBookText; //*[@id=\"avaliableFormatPanelGrp-heading-1\"]/div/a/div[1]/div[1]
	
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-body-1\"]/div/div[2]/div/button")	
	WebElement buttonText; //*[@id=\"avaliableFormatPanelGrp-body-1\"]/div/div/div[1]/div/button         
	
	
	//button[contains(@class,'[object Object]')]
	
	// Initializing Web Driver and PageFactory.
	
	public BookDetailsPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}

	
	// Call ReadJsonobject method to read and set node values from Json testData file. 
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();
	
	
	
	// Capture and return ISBN Number as input from Jason.
	
	@Step("Return ISBN Number from JSON file,  Method: {method} ")
	public String getISBNNumber() throws IOException, JsonIOException, JsonParseException, FileNotFoundException, InterruptedException {
		
		String ISBNText = jsonObj.getAsJsonObject("ShopBookName").get("InputISBN").getAsString();
		return ISBNText;
		
	}
	
	
	// Capture Book Heading in Book Details page.
	
	@Step("Get Book Title from Book Details,  Method: {method} ")
	 public String getBookHeading() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(bookTitle));
		String Title = bookTitle.getText();
		return Title;
		
	}
	
	 
	// Click on About the Book tab in Book Details page.
	
	@Step("Navigate to About The Book section,  Method: {method} ")
	public void clickAboutTheBook() throws Exception {
		
		//WebDriverWait wait = new WebDriverWait(driver,3);
		//TimeUnit.SECONDS.sleep(3);
		//wait.until(ExpectedConditions.visibilityOf(AboutTheBook));
		Thread.sleep(3000);
		AboutTheBook.click();
		
	}
	
	
	// Click on About the Author tab in Book Details page.
	
	@Step("Navigage to About The Author section,  Method: {method} ")
	public void clickAboutTheAuthor() throws Exception {
		
		//WebDriverWait wait = new WebDriverWait(driver,3);
		//TimeUnit.SECONDS.sleep(3);
		//wait.until(ExpectedConditions.visibilityOf(AuthorName));
		Thread.sleep(3000);
		AuthorName.click();
		
	}
	
	// Click on Product Details tab in About the Book section.
	
	@Step("Navigate to Product Details section,  Method: {method} ")
	public void clickProductDetails() throws Exception {
		
		//WebDriverWait wait = new WebDriverWait(driver,3);
		//TimeUnit.SECONDS.sleep(3);
		//wait.until(ExpectedConditions.visibilityOf(ProductDetails));
		Thread.sleep(3000);
		ProductDetails.click();
		
	}
	
	// Capture and return ISBN Number associated to Book from Product Details section.
	
	@Step("Get ISBN Number from Product Details section,  Method: {method} ")
	public String getISBN() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(ISBN));
		return ISBN.getText();
		
	}
	
	
	// Verify Pre Order button is displayed for NYP books.
	
	@Step("Validate Pre Order button for NYP,  Method: {method} ")
	public String NYPBookExist() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(NYPText));
		wait.until(ExpectedConditions.visibilityOf(eBookText));
		
		boolean NYPBookExist = true;
		String NYPBookTitle = null;
		String eBookTitle = null;
		String buttonTitle = null;
		
		try 
		{
			NYPBookExist = NYPText.isDisplayed();
			System.out.println(NYPBookExist);
			
			NYPBookTitle = NYPText.getText();
			System.out.println(NYPBookTitle);
			
			eBookTitle = eBookText.getText(); 
			System.out.println(eBookTitle);
			
		}
		
		catch (NoSuchElementException e)
		{
			NYPBookExist = false;
		}
		
		
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
		
		
		if((NYPBookExist && NYPBookTitle.equalsIgnoreCase("Not Yet Published")) && eBookTitle != ("Ebook & Learning Tools"))
		{
			
			buttonTitle = buttonText.getText();
			System.out.println(buttonTitle);
			
		}
		
		else
		{ 
			throw new NoSuchElementException("Error: Pre Order button is NOT displayed. ");

		}
			
			
		return buttonTitle;
		
	}
	
	
	// Verify Pre Order button is NOT displayed for NYP + eBooks.
	
		@Step("Validate Pre Order button for NYP + eBooks,  Method: {method} ")
		public boolean NYPeBookExist(String locator) throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			wait.until(ExpectedConditions.visibilityOf(NYPText));
			wait.until(ExpectedConditions.visibilityOf(eBookText));
			
			boolean NYPeBookExist = true;
			String NYPeBookTitle = null;
			String eBookTitle = null;
			boolean buttonExist = false;
			
			try 
			{
				NYPeBookExist = NYPText.isDisplayed();
				System.out.println(NYPeBookExist);
				
				NYPeBookTitle = NYPText.getText();
				System.out.println(NYPeBookTitle);
				
				eBookTitle = eBookText.getText(); 
				System.out.println(eBookTitle);
				
			}
			
			catch (NoSuchElementException e)
			{
				NYPeBookExist = false;
			}
			
			
			catch (Exception e) 
			{
				
				e.printStackTrace();
			}
			
			
			if((NYPeBookExist && NYPeBookTitle.equalsIgnoreCase("Not Yet Published")) && eBookTitle.equalsIgnoreCase ("Ebook & Learning Tools"))
			{
				try 
				{
					buttonExist = buttonText.isDisplayed();
				
					if(buttonExist)
					{
						buttonExist = true;
					}
				
			    } catch (NullPointerException | NoSuchElementException e) {
			        System.err.println("Unable to locate Any Button '" + locator + "'");
			        
			    } catch (Exception e) {
			        System.err.println("Unable to check display status of Button '" + locator + "'");
			        e.printStackTrace();
			    }
				
			}
			
			return buttonExist;
			
		}
		
	
		// Verify View All Options button is displayed for E-Item Book.
		
		@Step("Validate View All Options button for NYP + eBooks,  Method: {method} ")
		public String eItemBookExist() throws Exception {
			
			//WebDriverWait wait = new WebDriverWait(driver,3);
			//TimeUnit.SECONDS.sleep(3);
			//wait.until(ExpectedConditions.visibilityOf(NYPText));
			//wait.until(ExpectedConditions.visibilityOf(eBookText));
			
			Thread.sleep(3000);
			
			boolean eItemBookExist = true;
			String eItemBookTitle = null;
			String eBookTitle = null;
			String buttonTitle = null;
			
			try 
			{
				eItemBookExist = NYPText.isDisplayed();
				System.out.println(eItemBookExist);
				
				eItemBookTitle = NYPText.getText();
				System.out.println(eItemBookTitle);
				
				eBookTitle = eBookText.getText(); 
				System.out.println(eBookTitle);
				
			}
			
			catch (NoSuchElementException e)
			{
				eItemBookExist = false;
			}
			
			
			catch (Exception e) 
			{
				
				e.printStackTrace();
			}
			
			
			if((eItemBookExist && eItemBookTitle.contains("E-Item")) && eBookTitle.equalsIgnoreCase ("Ebook & Learning Tools"))
			{
				
				buttonTitle = buttonText.getText();
				System.out.println(buttonTitle);
				
			}
			
			else
			{ 
				throw new NoSuchElementException("Error: View All Options button is NOT displayed. ");

			}
			
			return buttonTitle;
			
		}
	
		
	
	// Click on Book binder options to make Add to Cart visible in Book Details page.
	
	@Step("Click on Book Binding options to see Add to Cart button,  Method: {method} ")
	public void addToCart() throws Exception 
		
	{
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		boolean addtoCartExist = true;
		String ButtonLabel = null;
		
		try 
		{
			
			Thread.sleep(3000);
			addtoCartExist = addtoCart1.isDisplayed();
			
			Thread.sleep(3000);
			ButtonLabel = addtoCart1.getText();
			System.out.println("Button Label: " + ButtonLabel);
		}
		
		catch (NoSuchElementException e) 
		{
			
			addtoCartExist = false;
		}
		
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
		
		
		if(addtoCartExist && (ButtonLabel != null) && (ButtonLabel.equals("Add To Cart") || ButtonLabel.equals("Preorder")))
		{
			
			Thread.sleep(1000);
			addtoCart1.click();
				
		}
			
		else
		{ 
			
			WebElement bindingCheckOutOption;
			List <WebElement> bindingOptions;
			Thread.sleep(3000);
			bindingOptions = driver.findElements(By.className("headerTitle"));
			 
			String[] elementArray  = new String[bindingOptions.size()];
			 
			 for(int i=1; i <bindingOptions.size(); i++) 
			 {  
				 elementArray[i-1] = bindingOptions.get(i).getText();
				 
			 }
			 
			 Thread.sleep(3000);
			 bindingCheckOutOption = driver.findElement(By.xpath("//div[contains(text()," + "'" + elementArray[0] + "')]"));
			 //System.out.println(elementArray[0]);
			
			 wait.until(ExpectedConditions.visibilityOf(bindingCheckOutOption));
			 bindingCheckOutOption.click();
			 
			 wait.until(ExpectedConditions.visibilityOf(addtoCart2));
			 addtoCart2.click();

		}
	}

}

