package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.objectFactory.PromotionalAddToCartPage;
import com.NortonWebsite.objectFactory.SearchBook;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })



public class MixShippingValidationTest extends PropertiesFile {
	
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		
		
		// Call to Properties file initiate Browser and set Test URL.
		
		public void callPropertiesFile() throws Exception {
			
			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setURL();
		
		}

		// Verify Free Shipping method when multiple items present in the cart after applying promo code in Checkout page.
		
		// Allure  Annotations

		@Severity(SeverityLevel.CRITICAL)
		@Description("Validate Free Shiiping method in Checkout page")
		@Stories("Add to Cart")
		@Test
		
		// Call to Add to Cart methods and Free Shipping Validation.
		
		public void MixShippingValidation() throws Exception {
			
			SearchBook InputText = PageFactory.initElements(driver, SearchBook.class);		
			InputText.setSearchText2("SearchText2");
			
			SearchBook SearchIconClick = PageFactory.initElements(driver, SearchBook.class);
			SearchIconClick.clickSearchIcon();
			
			BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
			bsr.clickFirstBook();
			
			// Add a book in Checkout side panel.
			BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
			viewAddToCart.addToCart();
			
			// Clicking on Checkout button and Login to Checkout side panel as an existing user.
			AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
			Cart.clickCheckout();
			Cart.accountLogin();
			
			// Apply promo code "BERGIN" in Checkout page.
			PromotionalAddToCartPage PromoCart = PageFactory.initElements(driver, PromotionalAddToCartPage.class);
			PromoCart.applyPromoCode2();
			
			CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
			
			// Verify that Free shipping option is displayed but NOT selected in Checkout page.
			boolean[] FreeShippingValue = cs.freeShippingExist(null);
			Assert.assertEquals(FreeShippingValue[0], true);
			Assert.assertEquals(FreeShippingValue[1], false);
			
			// Get all shipping options available in Shipping method in Checkout page.
			String[] strSMList = cs.shippingMethodList(null);
			Assert.assertTrue((strSMList[1].contains("Standard Shipping (Free)")));
			Assert.assertTrue((strSMList[2].contains("UPS Ground")));
			Assert.assertTrue((strSMList[3].contains("UPS Second Day")));
			Assert.assertTrue((strSMList[4].contains("UPS Next Day")));
			
			// Validate that Shipping tax value is displayed with "$0.00" in Checkout page.
			String strFreeShippingTaxValue = cs.getFreeShippingTaxValue();
			Assert.assertTrue((strFreeShippingTaxValue.equals("$0.00")));
			
			NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
			hp.clickHomeImage();
			
			// Add one more book in Checkout page.
			InputText.setSearchText1("SearchText1");
			SearchIconClick.clickSearchIcon();
			bsr.clickFirstBook();
			
			viewAddToCart.addToCart();
			Cart.clickCheckout();
			
			cs.notStudentInfo();
			
			// Verify that none of the shipping method is selected on adding one more book in Checkout page.
			Thread.sleep(3000);
			boolean[] isShippingMethodsSelected = cs.shippingMethodListSelected(null);
			Assert.assertEquals(isShippingMethodsSelected[1], false);
			Assert.assertEquals(isShippingMethodsSelected[2], false);
			Assert.assertEquals(isShippingMethodsSelected[3], false);
			
			String[] strJsonShippingPriceArray = cs.getShippingPrice();
			
			// Verify that discounted shipping price is displayed correctly in Checkout page.
			String[] strshippingPriceArray = cs.getDiscountedShipping();
			Assert.assertTrue((strshippingPriceArray[0].contains(strJsonShippingPriceArray[0])));
			Assert.assertTrue((strshippingPriceArray[1].contains(strJsonShippingPriceArray[1])));
		
		}
		
		
		// Closing driver and test.

		@AfterTest	
		public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();
		
		}

}
