package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartPayment;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.objectFactory.OrderConfirmationPage;
import com.NortonWebsite.utilities.PropertiesFile;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.NortonWebsite.utilities.TestListener;


// Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class AddToCartTest_ExistingUser extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
	
	}

	// Verify that Existing user logs into Checkout and places an Textbook order successfully.
	
	// Allure  Annotations

	@Severity(SeverityLevel.BLOCKER)
	@Description("Verify Add to cart with an Existing user")
	@Stories("Add to Cart")
	@Test
	
	// Call to Add to Cart methods.
	
	public void addtoCart() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopTextBooks();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectTextBook();
		
		// Add a book in Cart page.
		BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
		viewAddToCart.addToCart();
		
		// Clicking on Checkout button and Login to Checkout side panel as an existing user.
		AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
		Cart.clickCheckout();
		Cart.accountLogin();
		
		// Verify that existing Shipping information populated and select first shipping method.
		CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
		cs.shippingInfo();
		cs.shippingMethod();
		
		// Enter Payment information and place an order.
		CartPayment cp = PageFactory.initElements(driver, CartPayment.class);
		cp.enterPaymentInfo();
		cp.billingAddress();
		cp.checkTnC();
		cp.placeOrder();
		
		// Validate the order details in Order Confirmation page. 
		OrderConfirmationPage order = PageFactory.initElements(driver, OrderConfirmationPage.class);
		order.orderDetails();
		
		
	}

	
	// Closing driver and test.

	@AfterTest	
	public void closeTest() throws Exception {

	PropertiesFile.tearDownTest();
	
	}
	
}
