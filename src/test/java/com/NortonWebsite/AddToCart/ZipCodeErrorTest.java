package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartPayment;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class ZipCodeErrorTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
	
	}
	
	// Verify Zip Code error message in Shipping and Billing Information in Checkout page.

	// Allure  Annotations

	@Severity(SeverityLevel.BLOCKER)
	@Description("Validate Shipping Charges based on ISBN, State/Zip and Shipping Method")
	@Stories("Add to Cart")
	@Test
	
	// Call to Add to Cart methods.
	
	public void zipCodeError() throws Exception {
		
		// Click on Shop Textbook button in Norton Home page.
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopTextBooks();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectTextBook();
		
		// Add a book in Checkout side panel.
		BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
		viewAddToCart.addToCart();
		
		// Clicking on Checkout button and Login to Checkout side panel as an existing user.
		AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
		Cart.clickCheckout();
		Cart.accountLogin();
		
		CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
		cs.shippingInfo();
		
		// Remove the existing Shipping zip code.
		cs.removeShippingZipCode();
		
		//Enter Invalid Zip code NOT matching with State.
		cs.enterShippingZipCode2();
		cs.shippingMethod();
		
		// Verify that Zip Code error message is displayed correctly in Checkout page.
		String strShippingZipCodeErr = cs.shippingZipCodeErr();
		Assert.assertTrue(strShippingZipCodeErr.equals("Please verify state or zipcode."));
		
		cs.removeShippingZipCode();
		cs.enterShippingZipCode1();
		cs.shippingMethod();
		
		CartPayment cp = PageFactory.initElements(driver, CartPayment.class);
		cp.enterPaymentInfo();
		cp.enterBillingInfo();
		
		// Remove entered Zip Code in Billing Information.
		cp.removeBillingZipCode();
		
		//Enter Invalid Zip code NOT matching with State. 
		cp.enterBillingZipCode();
		cp.checkTnC();
		cp.placeOrder();
		
		// Verify that Zip Code error message is displayed correctly in Checkout page.
		String strBillingZipCodeErr = cp.billingZipCodeErr();
		Assert.assertTrue(strBillingZipCodeErr.equals("Please verify state or zipcode."));
		
	}
	
	
	// Closing driver and test.
	
	@AfterTest	
	public void closeTest() throws Exception {
		
	PropertiesFile.tearDownTest();
	
	}

}
