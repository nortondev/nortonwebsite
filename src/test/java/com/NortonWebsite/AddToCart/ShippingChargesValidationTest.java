package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartItemDetails;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class ShippingChargesValidationTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
	
	}
	
	// Validate Shipping Charges based on ISBN, State/Zip and Shipping Method in Checkout page.
	
	// Allure  Annotations

	@Severity(SeverityLevel.BLOCKER)
	@Description("Validate Shipping Charges based on ISBN, State/Zip and Shipping Method")
	@Stories("Add to Cart")
	@Test
	
	// Call to Add to Cart methods.
	
	public void shippingChargesValidation() throws Exception {
		
		// Click on Shop Book button in Norton Home page.
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopBooks();
		
		// Select the Book from the list based on Jason Input.
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectBook();
		
		// Add a book in Checkout side panel.
		BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
		viewAddToCart.addToCart();
		
		// Click on Checkout button in Side Checkout panel.
		AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
		Cart.clickCheckout();
		
		// Create New User Account in Checkout side panel.
		String strConfirmation = Cart.shippingAccount();
		Assert.assertTrue(strConfirmation.equals("Confirmation"));
		
		Cart.continueCheckout();
		
		CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
		cs.shippingInfo();
		
		// Get Shipping price for NY State address from the Json file.
		String[] strShippingPriceArrayNY = cs.getShippingPrice();
		
		// Get NY Shipping Tax value for the selected Shipping option in Checkout page.
		String strUPSShippingTaxValueNY = cs.getUPSShippingTaxValue();
		
		// Validate Json price with the Shipping tax of the selected Shipping option.
		Assert.assertTrue((strUPSShippingTaxValueNY.equals(strShippingPriceArrayNY[2])));
		
		// Update the Shipping Information
		cs.updateShipping();
					
		// Now update the Zip Code to NJ state.
		cs.updateZipCode();
		
		// Get Shipping price for NJ State address from the Json file.
		String[] strShippingPriceArrayFL = cs.getShippingPrice();
		
		// Get NJ Shipping Tax value for the selected Shipping option in Checkout page.
		String strUPSShippingTaxValueFL = cs.getUPSShippingTaxValue();
		
		// Validate Json price with the Shipping tax of the selected Shipping option.
		Assert.assertTrue((strUPSShippingTaxValueFL.equals(strShippingPriceArrayFL[3])));
		
		// Now Change the Quantity for the added item in Checkout page.
		CartItemDetails cid = PageFactory.initElements(driver, CartItemDetails.class);
		cid.selectShippingQuantity();
		
		// Get Shipping price for the updated Quantity from the Json file.
		String[] strShippingPriceArrayFLQty = cs.getShippingPrice();
		
		// Get Shipping Tax value for the updated Quantity in Checkout page.
		String strUPSShippingTaxValueFLQty = cs.getUPSShippingTaxValue();
		
		// Validate Json price with the Shipping tax of the selected Shipping option.
		Assert.assertTrue((strUPSShippingTaxValueFLQty.equals(strShippingPriceArrayFLQty[4])));
		
	}

}
