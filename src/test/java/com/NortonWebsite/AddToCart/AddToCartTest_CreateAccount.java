package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartPayment;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.objectFactory.OrderConfirmationPage;
import com.NortonWebsite.utilities.PropertiesFile;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.NortonWebsite.utilities.TestListener;


// Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class AddToCartTest_CreateAccount extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
	
	}

	// Verify that New user creates an account and places an book order successfully.
	
	// Allure  Annotations

	@Severity(SeverityLevel.BLOCKER)
	@Description("Validate Create Account with Add to Cart")
	@Stories("Add to Cart")
	@Test
	
	// Call to Add to Cart methods.
	
	public void createAccountAddtoCart() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopBooks();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectBook();
		
		// Add a book in Cart page.
		BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
		viewAddToCart.addToCart();
		
		// Navigate to Checkout side panel.
		AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
		Cart.clickCheckout();
		
		// Create an New User account and verify the Account Confirmation message.
		String strConfirmation = Cart.createAccount();
		System.out.println("Create Account Message : " + strConfirmation);
		Assert.assertTrue(strConfirmation.equals("Confirmation"));
		
		// Navigate to Checkout page.
		Cart.continueCheckout();
		
		// Enter Shipping Information and select first shipping method in Checkout page.
		CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
		cs.shippingInfo();
		cs.shippingMethod();
		
		// Enter Payment information and place an order.
		CartPayment cp = PageFactory.initElements(driver, CartPayment.class);
		cp.enterPaymentInfo();
		cp.billingAddress();
		cp.checkTnC();
		cp.placeOrder();
		
		// Validate the order details in Order Confirmation page. 
		OrderConfirmationPage order = PageFactory.initElements(driver, OrderConfirmationPage.class);
		order.orderDetails();
		
		boolean addressValue = order.orderShippingAddress();
		Assert.assertEquals(true, addressValue, "Shipping Address is displayed");

	}

	
	// Closing driver and test.

	@AfterTest	
	public void closeTest() throws Exception {

	Thread.sleep(3000);	
	PropertiesFile.tearDownTest();
	
	}
	
}