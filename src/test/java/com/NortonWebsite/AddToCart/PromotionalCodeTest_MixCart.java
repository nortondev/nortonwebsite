package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartPayment;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.objectFactory.OrderConfirmationPage;
import com.NortonWebsite.objectFactory.PromotionalAddToCartPage;
import com.NortonWebsite.objectFactory.PromotionalLandingPage;
import com.NortonWebsite.objectFactory.SearchBook;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class PromotionalCodeTest_MixCart extends PropertiesFile {

	// TestNG Annotations
			@Parameters("Browser")
			@BeforeTest
			
			// Call to Properties file initiate Browser and set Test URL.
			
			public void callPropertiesFile() throws Exception {
				
				PropertiesFile.readPropertiesFile();
				PropertiesFile.setBrowserConfig();
				PropertiesFile.setPromotionalURL();
			
			}
			
			// Verify that promo code is applied only for Promotional book when added promotional book along with Non promotional book in Checkout page. 
			
			// Allure  Annotations

			@Severity(SeverityLevel.CRITICAL)
			@Description("Verify Promo Code is Applied by default When cart has mixed items")
			@Stories("Add to Cart")
			@Test
			
			// Calls to Promotional Add to Cart flow.
			
			public void promoCodeBookDetails() throws Exception {
				
				// Get Original price and Discounted price on Promotional landing page.
				PromotionalLandingPage PromoPage = PageFactory.initElements(driver, PromotionalLandingPage.class);
				String strSellPrice = PromoPage.getSellPrice();
				String strDiscountPrice = PromoPage.getDiscountPrice();
				
				// Click on Add to Cart button against promotional book.
				PromoPage.clickPromotionalAddtoCart();
				
				// Clicking on Checkout button and Login to Checkout side panel as an existing user.
				AddToCart PromoCart = PageFactory.initElements(driver, AddToCart.class);
				PromoCart.clickCheckout();
				PromoCart.accountLogin();
				
				// Navigate to Norton Home page.
				NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
				hp.clickHomeImage();
				
				// Search for Non Promotional book.
				SearchBook InputText = PageFactory.initElements(driver, SearchBook.class);		
				InputText.setSearchText2("SearchText2");
			
				SearchBook SearchIconClick = PageFactory.initElements(driver, SearchBook.class);
				SearchIconClick.clickSearchIcon();
				
				// Select first Non Promotional book from the Book search Result list.
				BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
				bsr.clickFirstBook();
				
				// Click on Add to Cart and then Checkout button.
				BookDetailsPage AddtoCart = PageFactory.initElements(driver, BookDetailsPage.class);
				AddtoCart.addToCart();
				PromoCart.clickCheckout();
				
				PromotionalAddToCartPage PromoAddToCart = PageFactory.initElements(driver, PromotionalAddToCartPage.class);
				
				// Validate Promo code exist and promo code text box is not displayed in Checkout page.
				boolean PromoTextExist = PromoAddToCart.promoText();
				boolean PromoCodeBoxExist = PromoAddToCart.promoCodeBox();
				
				Assert.assertEquals(PromoTextExist, true);
				Assert.assertEquals(PromoCodeBoxExist, false);
				
				// Remove the promo code in Checkout page.
				PromoAddToCart.removePromoCode();
				
				// Validate Promo code is NOR displayed and promo code text box exist in Checkout page.
				boolean PromoTextEnabled = PromoAddToCart.promoText();
				boolean PromoCodeBoxHidden = PromoAddToCart.promoCodeBox();
				
				Assert.assertEquals(PromoTextEnabled, false);
				Assert.assertEquals(PromoCodeBoxHidden, true);
				
				// Apply promo code "BERGIN" in Checkout page.
				PromoAddToCart.applyPromoCode2();
				
				// Validate Promo code exist and promo code text box is not displayed in Checkout page.
				boolean PromoTextDisplayed = PromoAddToCart.promoText();
				boolean PromoCodeBoxInvisible = PromoAddToCart.promoCodeBox();
				
				Assert.assertEquals(PromoTextDisplayed, true);
				Assert.assertEquals(PromoCodeBoxInvisible, false);
				
				// Get Promo code in Checkout page.
				String strPromoCode = PromoAddToCart.getPromoCode();
				String JsonPromoCode = PromoAddToCart.getCode2();
				
				Assert.assertTrue(strPromoCode.contains(JsonPromoCode));
				
				//String strSubTotal = PromoAddToCart.getSubTotal();
				//String strTotal = PromoAddToCart.getTotal();
				
				//Assert.assertTrue(strDiscountPrice.equals(strTotal));
				//Assert.assertTrue(strSellPrice.equals(strSubTotal));
				
	
				CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
				cs.shippingInfo();
				
				// Verify that Free Shipping option is NOT displayed for mix items in Checkout page.
				boolean ShippingTaxOption = PromoAddToCart.getFreeShippingOption();
				Assert.assertEquals(ShippingTaxOption, false);
				
				cs.shippingMethod();
				
				CartPayment cp = PageFactory.initElements(driver, CartPayment.class);
				cp.enterPaymentInfo();
				cp.billingAddress();
				cp.checkTnC();
				cp.placeOrder();
				
				OrderConfirmationPage order = PageFactory.initElements(driver, OrderConfirmationPage.class);
				order.orderDetails();
				
			}
			
			
			// Closing driver and test.

			@AfterTest	
			public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();
			
			}

	}
