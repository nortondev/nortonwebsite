package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartPayment;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.objectFactory.OrderConfirmationPage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })



public class CheckoutPaymentValidationTest extends PropertiesFile {
	
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		
		
		// Call to Properties file initiate Browser and set Test URL.
		
		public void callPropertiesFile() throws Exception {
			
			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setURL();
		
		}

		// Validate the error messages for the blank values in Shipping Method, Payment and Billing Information and TnC checkbox in Checkout page.
		
		// Allure  Annotations

		@Severity(SeverityLevel.BLOCKER)
		@Description("Verify Add to cart with an Existing user")
		@Stories("Add to Cart")
		@Test
		
		// Call to Add to Cart methods to validate payment information errors.
		
		public void addtoCart() throws Exception {
			
			NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
			hp.clickShopBooks();
			
			BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
			bsr.selectBook();
			
			// Add a book in Checkout side panel.
			BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
			viewAddToCart.addToCart();
			
			// Clicking on Checkout button and Login to Checkout side panel as an existing user.
			AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
			Cart.clickCheckout();
			Cart.accountLogin();
			
			CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
			cs.submitShippingMethod();
			
			// Validate error message for Shipping method if not selected.
			String strShippingMethodErr = cs.shippingMethodErr();
			
			Assert.assertTrue(strShippingMethodErr.equals("This field is required."));
			
			cs.shippingMethod();
			
			// Validate error messages for blank values for Payment Information.
			CartPayment cp = PageFactory.initElements(driver, CartPayment.class);
			cp.placeOrder();
			
			String[] strPaymentError = cp.paymentErr();
			
			Assert.assertTrue((strPaymentError[0].equals("This field is required.")));
			Assert.assertTrue((strPaymentError[1].equals("This field is required.")));
			Assert.assertTrue((strPaymentError[2].equals("This field is required.")));
			Assert.assertTrue((strPaymentError[3].equals("This field is required.")));
			Assert.assertTrue((strPaymentError[4].equals("This field is required.")));
			
			
			// Validate error messages for blank values for Billing Information.
			String[] strBillingError = cp.billingErr();
			
			Assert.assertTrue((strBillingError[0].equals("This field is required.")));
			Assert.assertTrue((strBillingError[1].equals("This field is required.")));
			Assert.assertTrue((strBillingError[2].equals("This field is required.")));
			Assert.assertTrue((strBillingError[3].equals("This field is required.")));
			Assert.assertTrue((strBillingError[4].equals("This field is required.")));
			Assert.assertTrue((strBillingError[5].equals("This field is required.")));
			Assert.assertTrue((strBillingError[6].equals("This field is required.")));
			
			// Validate error messages for blank values for TnC Checkbox.
			String strTnCError = cp.errorTnC();
			Assert.assertTrue(strTnCError.equals("Please accept terms."));
			
			// Enter valid payment information and billing information and verify that order get placed successfully.
			cp.enterPaymentInfo();
			cp.enterBillingInfo();
			cp.checkTnC();
			cp.placeOrder();
			
			OrderConfirmationPage order = PageFactory.initElements(driver, OrderConfirmationPage.class);
			order.orderDetails();
			
		}
	
			// Closing driver and test.

			@AfterTest	
			public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();
			
			}

}
