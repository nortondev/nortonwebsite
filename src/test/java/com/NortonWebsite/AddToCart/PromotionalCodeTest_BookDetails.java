package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.CartPayment;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.OrderConfirmationPage;
import com.NortonWebsite.objectFactory.PromotionalAddToCartPage;
import com.NortonWebsite.objectFactory.PromotionalBookDetailsPage;
import com.NortonWebsite.objectFactory.PromotionalLandingPage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class PromotionalCodeTest_BookDetails extends PropertiesFile {
	

	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		
		// Call to Properties file initiate Browser and set Test URL.
		
		public void callPropertiesFile() throws Exception {
			
			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setPromotionalURL();
		
		}
		
		// Verify that promo code is applied correctly in Checkout page when navigate to Promotinal Book Details page with Promotional book url. 
		
		// Allure  Annotations

		@Severity(SeverityLevel.CRITICAL)
		@Description("Verify Promo Code is applied in the Cart")
		@Stories("Add to Cart")
		@Test
		
		// Calls to Promotional Add to Cart flow.
		
		public void promoCodeBookDetails() throws Exception {
			
			// Get Original price and Discounted price on Promotional landing page.
			PromotionalLandingPage PromoPage = PageFactory.initElements(driver, PromotionalLandingPage.class);
			String strSellPrice = PromoPage.getSellPrice();
			String strDiscountPrice = PromoPage.getDiscountPrice();
			
			PromotionalBookDetailsPage PromoBookDetailsPage = PageFactory.initElements(driver, PromotionalBookDetailsPage.class);
			PromoBookDetailsPage.clickFirstBookLink();
			
			// Click on Add to Cart button against promotional book in Book Details page.
			BookDetailsPage AddtoCart = PageFactory.initElements(driver, BookDetailsPage.class);
			AddtoCart.addToCart();
			
			// Clicking on Checkout button and Login to Checkout side panel as an existing user.
			AddToCart PromoCart = PageFactory.initElements(driver, AddToCart.class);
			PromoCart.clickCheckout();
			PromoCart.accountLogin();
			
			PromotionalAddToCartPage PromoAddToCart = PageFactory.initElements(driver, PromotionalAddToCartPage.class);
			
			// Get Book Original price and discounted price in Checkout page.
			String strCartOriginalPrice = PromoAddToCart.getCartOriginalPrice();
			String strCartDiscountedPrice = PromoAddToCart.getCartDiscountedPrice();
			
			// Get Sub Total amount in Checkout page. 
			String strSubTotal = PromoAddToCart.getSubTotal();
			
			// Get promo code text in Checkout page.
			String strPromoCode = PromoAddToCart.getPromoCode();
			
			// Get JSON Promo code from Json Test data file.
			String JsonPromoCode = PromoAddToCart.getCode1();
			
			// Validate Promo code exist and promo code text box is not displayed in Checkout page.
			boolean PromoTextExist = PromoAddToCart.promoText();
			boolean PromoCodeBoxExist = PromoAddToCart.promoCodeBox();
			
			Assert.assertEquals(PromoTextExist, true);
			Assert.assertEquals(PromoCodeBoxExist, false);
			
			// Compare Book Original price with Cart Original price.
			Assert.assertTrue(strSellPrice.equals(strCartOriginalPrice));
			
			// Compare Book Discounted price with Cart Discounted price.
			Assert.assertTrue(strDiscountPrice.equals(strCartDiscountedPrice));
			
			// Compare Book Discounted price with Cart Sub Total Amount.
			Assert.assertTrue(strDiscountPrice.equals(strSubTotal));
			
			// Verify that promo code is matching with Json promo code.
			Assert.assertTrue(strPromoCode.contains(JsonPromoCode));
			
			CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
			cs.shippingInfo();
			
			// Verify that Free Shipping option is displayed when promo code is applied.
			boolean ShippingTaxOption = PromoAddToCart.getFreeShippingOption();
			Assert.assertEquals(ShippingTaxOption, true);
			
			PromoAddToCart.clickFreeShippingOption();
			
			// Verify that Zero Estimated tax is displayed when Promo code is applied.
			boolean EstimatedTaxValue = PromoAddToCart.getEstimatedTax();
			Assert.assertEquals(EstimatedTaxValue, false);
			
			CartPayment cp = PageFactory.initElements(driver, CartPayment.class);
			cp.enterPaymentInfo();
			cp.billingAddress();
			cp.checkTnC();
			cp.placeOrder();
			
			OrderConfirmationPage order = PageFactory.initElements(driver, OrderConfirmationPage.class);
			order.orderDetails();
			
		}
		
		
		// Closing driver and test.

		@AfterTest	
		public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();
		
		}

}
