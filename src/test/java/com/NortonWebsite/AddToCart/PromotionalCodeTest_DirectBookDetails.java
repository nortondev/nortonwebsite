package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.CartPayment;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.OrderConfirmationPage;
import com.NortonWebsite.objectFactory.PromotionalAddToCartPage;
import com.NortonWebsite.objectFactory.PromotionalBookDetailsPage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class PromotionalCodeTest_DirectBookDetails extends PropertiesFile {

	// TestNG Annotations
			@Parameters("Browser")
			@BeforeTest
			
			// Call to Properties file initiate Browser and set Test URL.
			
			public void callPropertiesFile() throws Exception {
				
				PropertiesFile.readPropertiesFile();
				PropertiesFile.setBrowserConfig();
				PropertiesFile.setDirectURL();
			
			}
			
			// Verify that promo code is NOT applied by default in Checkout page when navigate to Book Details page directly. 
			
			// Allure  Annotations

			@Severity(SeverityLevel.CRITICAL)
			@Description("Verify Promo Code is Not Applied by default in the Cart")
			@Stories("Add to Cart")
			@Test
			
			// Calls to Promotional Add to Cart flow.
			
			public void promoCodeBookDetails() throws Exception {
				
				// Get Book price on Promotional Book Details page.
				PromotionalBookDetailsPage Price = PageFactory.initElements(driver, PromotionalBookDetailsPage.class);
				String OriginalBookPrice = Price.getBookPrice();
				
				// Click on Add to Cart button in Promotional Book Details page.
				BookDetailsPage AddtoCart = PageFactory.initElements(driver, BookDetailsPage.class);
				AddtoCart.addToCart();
				
				// Clicking on Checkout button and Login to Checkout side panel as an existing user.
				AddToCart PromoCart = PageFactory.initElements(driver, AddToCart.class);
				PromoCart.clickCheckout();
				PromoCart.accountLogin();
				
				PromotionalAddToCartPage PromoAddToCart = PageFactory.initElements(driver, PromotionalAddToCartPage.class);
				
				// Verify that Promo code is NOT displayed and promo code box exist when navigate to Checkout page from Promotional Book Details page.
				boolean PromoTextExist = PromoAddToCart.promoText();
				boolean PromoCodeBoxExist = PromoAddToCart.promoCodeBox();
				
				Assert.assertEquals(PromoTextExist, false);
				Assert.assertEquals(PromoCodeBoxExist, true);
				
				// Apply promo code "SPRING193" in Checkout page.
				PromoAddToCart.applyPromoCode1();
				
				// Get promo code after applying promo code in Checkout page.
				String strPromoCode = PromoAddToCart.getPromoCode();
				
				// Get Json Promo code from Json test data file.
				String JsonPromoCode = PromoAddToCart.getCode1();
				
				// Verify that Promo code is displayed and promo code box does NOT exist when promo code is applied.
				boolean PromoTextEnabled = PromoAddToCart.promoText();
				boolean PromoCodeBoxHidden = PromoAddToCart.promoCodeBox();
				
				Assert.assertEquals(PromoTextEnabled, true);
				Assert.assertEquals(PromoCodeBoxHidden, false);
				Assert.assertTrue(strPromoCode.contains(JsonPromoCode));
				
				// Get Sub Total and Total amount in Checkout page after applying promo code.
				String strSubTotal = PromoAddToCart.getSubTotal();
				
				// Calculate Discounted total and validate with Discounted price displayed in Checkout page.
				try 
				{
					
					float DiscountPriceValue = Float.valueOf(PromoAddToCart.getSubTotal())*25/100;
					String DiscountedTotal = String.valueOf(DiscountPriceValue);
					
					Assert.assertTrue(DiscountedTotal.equals(strSubTotal));
					
				} catch(NumberFormatException ex) {
					
					System.out.println("Price is not in proper format ");
				}
				
				// Validate Original Book price in Promotional Book Details page with Sub Total amount displayed in Checkout page.
				//Assert.assertTrue(OriginalBookPrice.equals(strSubTotal));
				
	
				CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
				cs.shippingInfo();
				
				// Verify that Free Shipping option is displayed when promo code is applied.
				boolean ShippingTaxOption = PromoAddToCart.getFreeShippingOption();
				Assert.assertEquals(ShippingTaxOption, true);
				
				cs.shippingMethod();
				
				CartPayment cp = PageFactory.initElements(driver, CartPayment.class);
				cp.enterPaymentInfo();
				cp.billingAddress();
				cp.checkTnC();
				cp.placeOrder();
				
				OrderConfirmationPage order = PageFactory.initElements(driver, OrderConfirmationPage.class);
				order.orderDetails();
				
			}
			
			
			// Closing driver and test.

			@AfterTest	
			public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();
			
			}

	}
