package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartItemDetails;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.NortonWebsite.utilities.TestListener;


// Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class RemoveCartItemTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
	
	}
	
	// Verify that user is able to remove the items from Cart successfully in Checkout page.

	// Allure  Annotations

	@Severity(SeverityLevel.BLOCKER)
	@Description("Verify Add to cart with an Existing user")
	@Stories("Add to Cart")
	@Test
	
	// Call to Add to Cart and Remove methods.
	
	public void addtoCart() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopTextBooks();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectTextBook();
		
		// Add a book in Checkout side panel.
		BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
		viewAddToCart.addToCart();
		
		// Clicking on Checkout button and Login to Checkout side panel as an existing user.
		AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
		Cart.clickCheckout();
		Cart.accountLogin();
		
		// Remove items from the cart and verify that "Your cart is empty." message is displayed correctly in Checkout page.
		CartItemDetails cid = PageFactory.initElements(driver, CartItemDetails.class);
		cid.clickRemove();
		String EmptyCart = cid.getEmptyCartText();
		
		Assert.assertTrue(EmptyCart.equals("Your cart is empty."));
		
	}

	
	// Closing driver and test.

	@AfterTest	
	public void closeTest() throws Exception {

	PropertiesFile.tearDownTest();
	
	}
	
}