package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartItemDetails;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class CheckoutPopUpTest extends PropertiesFile {
	
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
	
	}
	
	// Verify that the Checkout Pop Up is displayed when more than 5 copies are selected as Quantity in Checkout page.

	// Allure  Annotations

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Checkout Pop Up when more than 5 copies are selected")
	@Stories("Add to Cart")
	@Test
	
	// Call to Add to Cart class file.
	
	public void addtoCart() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopTextBooks();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectTextBook();
		
		// Add a book in Checkout side panel.
		BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
		viewAddToCart.addToCart();
		
		// Clicking on Checkout button and Login to Checkout side panel as an existing user.
		AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
		Cart.clickCheckout();
		Cart.accountLogin();
		
		// Select Quantity greater than 5 in Checkout page.
		CartItemDetails cid = PageFactory.initElements(driver, CartItemDetails.class);
		cid.selectQuantity();
		
		// Validate the information on Checkout Pop-up is displayed correctly.
		String[] strmodleMsg = cid.qtyPopUpMessage();
		Assert.assertTrue((strmodleMsg[0].equalsIgnoreCase("Multiple Quantities")));
		Assert.assertTrue((strmodleMsg[1].equalsIgnoreCase("If you are ordering multiple copies on behalf of a high school, organization, or institution, please contact our warehouse directly for appropriate discount, tax, and freight considerations.")));
		Assert.assertTrue((strmodleMsg[2].equalsIgnoreCase("Call: (800) 233-4830")));
		Assert.assertTrue((strmodleMsg[3].equalsIgnoreCase("Email: orders@wwnorton.com")));
		
		// Close Checkout pop-up.
		boolean closeButtonValue = cid.checkoutPopUpDisplay();
		Assert.assertEquals(true, closeButtonValue);

	}

	
	// Closing driver and test.

	@AfterTest	
	public void closeTest() throws Exception {

	PropertiesFile.tearDownTest();
	
	}
	
}