package com.NortonWebsite.AddToCart;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.NortonWebsite.utilities.TestListener;


// Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class StudentInfoNOTExistTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
	
	}

	// Verify that Student Panel is NOT displayed correctly when general book is added in Checkout page.
	
	// Allure  Annotations

	@Severity(SeverityLevel.BLOCKER)
	@Description("Verify Add to cart with an Existing user")
	@Stories("Add to Cart")
	@Test
	
	// Call to Add to Cart and Remove methods.
	
	public void addtoCart() throws Exception {
		
		// Click on Shop Book button in Norton Home page.
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopBooks();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectBook();
		
		// Add a book in Checkout side panel.
		BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
		viewAddToCart.addToCart();
		
		// Clicking on Checkout button and Login to Checkout side panel as an existing user.
		AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
		Cart.clickCheckout();
		Cart.accountLogin();
		
		// Verify that Student Panel is Not displayed in Checkout page.
		CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
		boolean SPValue = cs.studentInfoExist();
		Assert.assertEquals(false, SPValue, "Student Panel is NOT displayed");
		
	}

	
	// Closing driver and test.

	@AfterTest	
	public void closeTest() throws Exception {

	PropertiesFile.tearDownTest();
	
	}
	
}
