package com.NortonWebsite.ExamCopy;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.objectFactory.RequestExamCopy;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class RequestExamCopyTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
		
	}

	
//	Verify that "Free" text is displayed on Exam Copy Checkout page when Exam Copy is requested.
	
	// Allure  Annotations

	@Severity(SeverityLevel.BLOCKER)
	@Description("Verify 'Free' text on Exam Copy Checkout page")
	@Stories("Account Dashboard Page")
	@Test
	
	public void requestExamCopy() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopTextBooks();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selecteItemBook();
		
		// Click on Request Exam Copy button if Request Exam panel exist on Book Details page.
		RequestExamCopy rec = PageFactory.initElements(driver, RequestExamCopy.class);
		rec.clickRequestExamCopy(null);
		
		//Login as an existing user in Request Exam Copy workflow.
		rec.loginExamCopy();
		
		//Enter School Information in Request Exam Copy pop-up.
		rec.enterSchoolInformation();
		rec.enterCourseInformation();
		
	}
}
