package com.NortonWebsite.SearchBook;



import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })

public class BookSearchResultsTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
	}

	// Verify that "Show More" button is displayed at the bottom of the Book Search Results page.
	
	// Allure  Annotations

	@Severity(SeverityLevel.NORMAL)
	@Description("Validate Book Search Results")
	@Stories("Search Book")
	@Test
	
	// Call to methods for Clicking on Show More button at the bottom of the search results page.
	
	public void searchResults() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopBooks();
		
		// Validate Show More button at the bottom of the Book Search Results page.
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.showMoreExist();
			
	}
	
	// Closing driver and test.
	
	@AfterTest	
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();
		
	}
	
	
}
