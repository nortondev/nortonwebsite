package com.NortonWebsite.SearchBook;

import org.openqa.selenium.support.PageFactory;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.SearchBook;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })

public class SearchBookTest extends PropertiesFile {


	// TestNG Annotations
	@BeforeTest
		
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
			
			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setURL();
			
		}	
	
	// Verify that Search text displayed on Book Search Result page is matching with the entered search text.
	
	// Allure  Annotations

	@Severity(SeverityLevel.CRITICAL)
	@Description("Validate Book Title with Book Search Results")
	@Stories("Search Book")
	@Test
	
	// Call to methods to Search books based on Search key and validating Search title.
	
		public void searchBookTest() throws Exception {
			
			// Enter Search text in Search box.
			SearchBook InputText = PageFactory.initElements(driver, SearchBook.class);		
			InputText.setSearchText1("SearchText1");
			
			//SearchBook SearchIconClick = PageFactory.initElements(driver, SearchBook.class);
			InputText.clickSearchIcon();
			
			// Get Search Result text from Book Search result page.
			BookSearchResultsPage searchResultsText = PageFactory.initElements(driver, BookSearchResultsPage.class);
			String searchString = searchResultsText.getSearchResultsText();
			
			// Compare entered search text with the search text displayed on Book Search Result page.
		    Assert.assertTrue(searchString.contains(InputText.getSearchText()));
			
		}	
	
	
	// Closing driver and test.
	
	@AfterTest	
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();
			
		}
		
}	
