package com.NortonWebsite.AccountDashboard;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AccountDashboardPage;
import com.NortonWebsite.objectFactory.AddToCart;
import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.CartPayment;
import com.NortonWebsite.objectFactory.CartShipping;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.objectFactory.OrderConfirmationPage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class MultipleShippingTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
		
	}

//	Verify that more than one Shipping Addresses are exist in Account Dashboard page.
//	Verify that most recent updated shipping address in Account profile page is getting populated in Checkout page.
	
	// Allure  Annotations

	@Severity(SeverityLevel.NORMAL)
	@Description("Validate multiple shipping address in Checkout form")
	@Stories("Account Dashboard Page")
	@Test
	
	public void multipleShipping() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickMyAccount();
		hp.loginMyAccount();
		hp.clickProfile();
		
		// Verify that multiple shipping addresses exist in Account profile page.
		AccountDashboardPage myaccount = PageFactory.initElements(driver, AccountDashboardPage.class);
		boolean SDValues = myaccount.multipleAddresses();
		
		Assert.assertEquals(true, SDValues, "Multiple addresses exist");
		
		// Update the Shipping address in Account profile page.
		myaccount.editAddressTwo();
		
		String strShippingAddressOne = myaccount.getAddressOne();
		//System.out.println(strShippingAddressTwo);
		
		hp.clickCartItemsLink();
		hp.clickShopBookButton();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectBook();
		
		BookDetailsPage viewAddToCart = PageFactory.initElements(driver, BookDetailsPage.class);
		viewAddToCart.addToCart();
		
		AddToCart Cart = PageFactory.initElements(driver, AddToCart.class);
		Cart.clickCheckout();
		
		CartShipping cs = PageFactory.initElements(driver, CartShipping.class);
		cs.shippingMethod();
		
		CartPayment cp = PageFactory.initElements(driver, CartPayment.class);
		cp.enterPaymentInfo();
		cp.billingAddress();
		cp.checkTnC();
		cp.placeOrder();
		
		OrderConfirmationPage order = PageFactory.initElements(driver, OrderConfirmationPage.class);
		order.orderDetails();
		
		// Verify that most recently updated Shipping address in Account profile is getting populated in Checkout page and displayed in Order Confirmation page.
		String strShippingAddress = order.getShippingAddress();
		System.out.println(strShippingAddress);
		
		Assert.assertTrue(strShippingAddressOne.contains(strShippingAddress));
		
	}
	
	// Closing driver and test.
	
	@AfterTest	
	public void closeTest() throws Exception {
		
	PropertiesFile.tearDownTest();
	
	}

}

