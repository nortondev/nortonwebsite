package com.NortonWebsite.AccountDashboard;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class WebsiteLoginTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
		
	}

	
//	Verify Norton Website Login functionality.
	
	// Allure  Annotations

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Norton Account Login")
	@Stories("Account Login")
	@Test
	
	public void nortonAccountLogin() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		
		for(int i=1; i<=1000; i++) 
		{
			
			hp.clickMyAccount();
			hp.loginMyAccount();
			hp.clickProfile();
			
			String loggedInUser = hp.validateLoginUser();
			Assert.assertEquals(loggedInUser, "adewalkar@wwnorton.com");
			
			hp.clickMyAccount();
			hp.logOutWebsite();
		
		}
		
	}
	
	// Closing driver and test.
	
	@AfterTest	
	public void closeTest() throws Exception {
		
	PropertiesFile.tearDownTest();
	
	}

}
