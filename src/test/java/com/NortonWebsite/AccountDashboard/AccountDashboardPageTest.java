package com.NortonWebsite.AccountDashboard;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.AccountDashboardPage;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;




//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })

public class AccountDashboardPageTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
		
	}

	
//	Verify that Shipping Address is displayed in Account Dashboard after placing 
//	an Order with New User matching with entered Shipping Information in Checkout page.
	
	// Allure  Annotations

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Account Infomation")
	@Stories("Account Dashboard Page")
	@Test
	
	public void accountInformation() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickMyAccount();
		hp.loginMyAccount();
		hp.clickProfile();

		AccountDashboardPage myaccount = PageFactory.initElements(driver, AccountDashboardPage.class);
		
		// Get the shipping address from Json Test data file.
		String strJSONAddress = myaccount.getJSONAddress();
		
		//Get the saved shipping address in Account Profile page. 
		String strAddressOne = myaccount.getAddressOne();
		
		//Compare Shipping address in Jason file and Account profile page.
		Assert.assertTrue(strAddressOne.contains(strJSONAddress));
		
	}
	
	// Closing driver and test.
	
	@AfterTest	
	public void closeTest() throws Exception {
		
	PropertiesFile.tearDownTest();
	
	}

}
