package com.NortonWebsite.BookDetails;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class BookDetailsNYPeBookTest extends PropertiesFile {
	
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
		
	}

	// Verify that Pre Order button is displayed for NYP eBooks in Book Details page.
	
	// Allure  Annotations

	@Severity(SeverityLevel.CRITICAL)
	@Description("Validate Pre Order button for NYP Books in Book Details page")
	@Stories("Book Details Page")
	@Test


// Call to methods for Clicking on First book in the search result list and validating Book Title.

	public void booksList() throws Exception {
	
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopTextBooks();
	
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectNYPeBook();
		
		// Validate Pre Order button is displayed for NYP eBooks in Book Details page.
		BookDetailsPage bdp = PageFactory.initElements(driver, BookDetailsPage.class);
		boolean buttonValue = bdp.NYPeBookExist(null);
		
		Assert.assertEquals(false, buttonValue);
		
	}
	
	
	// Closing driver and test.

	@AfterTest	
	public void closeTest() throws Exception {
		
	PropertiesFile.tearDownTest();
	
	}
	
}
