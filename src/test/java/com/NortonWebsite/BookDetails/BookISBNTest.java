package com.NortonWebsite.BookDetails;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.NortonWebsite.objectFactory.BookDetailsPage;
import com.NortonWebsite.objectFactory.BookSearchResultsPage;
import com.NortonWebsite.objectFactory.NortonHomePage;
import com.NortonWebsite.utilities.PropertiesFile;
import com.NortonWebsite.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.

@Listeners({ TestListener.class })


public class BookISBNTest extends PropertiesFile {
	
	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest
	
	// Call to Properties file initiate Browser and set Test URL.
	
	public void callPropertiesFile() throws Exception {
		
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig();
		PropertiesFile.setURL();
		
	}

	// Verify that Book ISBN# in Book Details page is matching with ISBN# in JSON test data file.
	
	// Allure  Annotations

	@Severity(SeverityLevel.CRITICAL)
	@Description("Validate ISBN Number")
	@Stories("Book Details Page")
	@Test
	
	// Call to methods for Clicking on Product Details tab in Book Details page and validating ISBN Number.
	
	public void booksList() throws Exception {
		
		NortonHomePage hp = PageFactory.initElements(driver, NortonHomePage.class);
		hp.clickShopBooks();
		
		BookSearchResultsPage bsr = PageFactory.initElements(driver, BookSearchResultsPage.class);
		bsr.selectBook();
		
		BookDetailsPage bd = PageFactory.initElements(driver, BookDetailsPage.class);
		bd.clickAboutTheBook();
		bd.clickProductDetails();
		
		// Get ISBN# from JSON test data file.
		String strISBNText = bd.getISBNNumber();
		
		// Get ISBN# under Product section in Book Details page.
		BookDetailsPage ISBNDetails = PageFactory.initElements(driver, BookDetailsPage.class);
		String strISBN = ISBNDetails.getISBN();
		
		// Validate both ISBN#
		Assert.assertTrue(strISBN.contains(strISBNText));
		
	}

	
	// Closing driver and test.

	@AfterTest	
	public void closeTest() throws Exception {

	PropertiesFile.tearDownTest();
	
	}

}

